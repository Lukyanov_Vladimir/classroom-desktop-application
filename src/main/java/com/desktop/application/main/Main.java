package com.desktop.application.main;

import com.desktop.application.controllers.mainController.MainController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {

    private MainController mainController;
    private final EventHandler<WindowEvent> onCloseRequestAppBtnHandler = event -> mainController.shutdown();

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));

        mainController = new MainController();
        mainController.setPrimaryStage(primaryStage);

        fxmlLoader.setController(mainController);
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);

        primaryStage.setTitle("Приложение классного руководителя");
        primaryStage.centerOnScreen();
        primaryStage.setMinHeight(780);
        primaryStage.setMinWidth(1000);
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image("/img/icon.png"));
        primaryStage.setOnCloseRequest(onCloseRequestAppBtnHandler);
        primaryStage.show();
    }
}
