package com.desktop.application.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    /**
     * Методо создаёт фабрику сессий
     */
    public static void buildSessionFactory() {

        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure("cfg/hibernate.cfg.xml").build();

        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
            throw new ExceptionInInitializerError("Initial SessionFactory failed" + e);
        }

    }

    /**
     * Метод открывает новую сессию
     *
     * @return - новая сессия
     */
    public static Session openSession() {
        return sessionFactory.openSession();
    }

    /**
     * Метод возвращает фабрику сессий
     *
     * @return - фабрика сессий
     */
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Метод закрывает подключение к базе данных и фабрику сессий
     */
    public static void shutdown() {
        getSessionFactory().close();
    }
}