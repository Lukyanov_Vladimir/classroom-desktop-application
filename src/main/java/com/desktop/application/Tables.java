package com.desktop.application;

public enum Tables {
    STUDENTS,
    PASSPORTS_DATA,
    PERFORMANCES,
    ATTENDANCES,
    BIRTH_CERTIFICATES,
    PARENTS,
    DISCIPLINES;
}
