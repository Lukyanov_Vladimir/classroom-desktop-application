package com.desktop.application;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CreatingDate {

    /**
     * Метод создаёт объект дата
     *
     * @param date - строка с датой
     * @return - дата
     */
    public static Date createDate(String date) {

        String[] arrDateNumbers = splitDate(date);

        Calendar calendar = new GregorianCalendar(parseInt(arrDateNumbers[0]), parseInt(arrDateNumbers[1]),
                parseInt(arrDateNumbers[2]));

        Date newDate = new Date();
        newDate.setTime(calendar.getTimeInMillis());

        return newDate;
    }

    /**
     * Метод преобразует строку в целое число
     *
     * @param str - строка
     * @return - целое число
     */
    private static int parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Метод разбивает дату на числа
     *
     * @param date - дата
     * @return - массив с числами
     */
    private static String[] splitDate(String date) {
        return date.split("-");
    }
}
