package com.desktop.application.dao;

import com.desktop.application.entities.BirthCertificate;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class BirthCertificateDao implements Dao<BirthCertificate> {

    private final Session session;

    public BirthCertificateDao(Session session) {
        this.session = session;
    }

    @Override
    public BirthCertificate findById(int id) {
        return session.get(BirthCertificate.class, id);
    }

    @Override
    public List<BirthCertificate> findAll() {
        return (List<BirthCertificate>) session.createQuery("FROM BirthCertificate").list();
    }

    @Override
    public void save(BirthCertificate birthCertificate) {
        Transaction tr = session.beginTransaction();
        session.save(birthCertificate);
        tr.commit();
    }

    @Override
    public void update(BirthCertificate birthCertificate) {
        Transaction tr = session.beginTransaction();
        session.update(birthCertificate);
        tr.commit();
    }

    @Override
    public void delete(BirthCertificate birthCertificate) {
        Transaction tr = session.beginTransaction();
        session.delete(birthCertificate);
        tr.commit();
    }
}
