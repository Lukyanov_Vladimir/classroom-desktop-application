package com.desktop.application.dao;

import com.desktop.application.entities.Attendance;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class AttendanceDao implements Dao<Attendance> {

    private final Session session;

    public AttendanceDao(Session session) {
        this.session = session;
    }

    @Override
    public Attendance findById(int id) {
        return session.get(Attendance.class, id);
    }

    @Override
    public List<Attendance> findAll() {
        return session.createQuery("FROM Attendance").list();
    }

    @Override
    public void save(Attendance attendance) {
        Transaction tr = session.beginTransaction();
        session.save(attendance);
        tr.commit();
    }

    @Override
    public void update(Attendance attendance) {
        Transaction tr = session.beginTransaction();
        session.update(attendance);
        tr.commit();
    }

    @Override
    public void delete(Attendance attendance) {
        Transaction tr = session.beginTransaction();
        session.delete(attendance);
        tr.commit();
    }
}
