package com.desktop.application.dao;

import com.desktop.application.entities.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class StudentDao implements Dao<Student> {

    private final Session session;

    public StudentDao(Session session) {
        this.session = session;
    }

    @Override
    public Student findById(int id) {
        return session.get(Student.class, id);
    }

    @Override
    public List<Student> findAll() {
        return (List<Student>) session.createQuery("FROM Student").list();
    }

    @Override
    public void save(Student student) {
        Transaction tr = session.beginTransaction();
        session.save(student);
        tr.commit();
    }

    @Override
    public void update(Student student) {
        Transaction tr = session.beginTransaction();
        session.update(student);
        tr.commit();
    }

    @Override
    public void delete(Student student) {
        Transaction tr = session.beginTransaction();
        session.delete(student);
        tr.commit();
    }
}
