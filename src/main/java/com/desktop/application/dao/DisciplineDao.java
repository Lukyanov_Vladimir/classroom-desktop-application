package com.desktop.application.dao;

import com.desktop.application.entities.Discipline;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class DisciplineDao implements Dao<Discipline> {

    private final Session session;

    public DisciplineDao(Session session) {
        this.session = session;
    }

    @Override
    public Discipline findById(int id) {
        return session.get(Discipline.class, id);
    }

    @Override
    public List<Discipline> findAll() {
        return (List<Discipline>) session.createQuery("FROM Discipline").list();
    }

    @Override
    public void save(Discipline discipline) {
        Transaction tr = session.beginTransaction();
        session.save(discipline);
        tr.commit();
    }

    @Override
    public void update(Discipline discipline) {
        Transaction tr = session.beginTransaction();
        session.update(discipline);
        tr.commit();
    }

    @Override
    public void delete(Discipline discipline) {
        Transaction tr = session.beginTransaction();
        session.delete(discipline);
        tr.commit();
    }
}
