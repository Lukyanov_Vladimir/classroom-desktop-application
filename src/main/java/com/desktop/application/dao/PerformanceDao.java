package com.desktop.application.dao;

import com.desktop.application.entities.Performance;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class PerformanceDao implements Dao<Performance> {

    private final Session session;

    public PerformanceDao(Session session) {
        this.session = session;
    }

    @Override
    public Performance findById(int id) {
        return session.get(Performance.class, id);
    }

    @Override
    public List<Performance> findAll() {
        return (List<Performance>) session.createQuery("FROM Performance").list();
    }

    @Override
    public void save(Performance performance) {
        Transaction tr = session.beginTransaction();
        session.save(performance);
        tr.commit();
    }

    @Override
    public void update(Performance performance) {
        Transaction tr = session.beginTransaction();
        session.update(performance);
        tr.commit();
    }

    @Override
    public void delete(Performance performance) {
        Transaction tr = session.beginTransaction();
        session.delete(performance);
        tr.commit();
    }
}
