package com.desktop.application.dao;

import com.desktop.application.entities.Parent;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ParentDao implements Dao<Parent> {

    private final Session session;

    public ParentDao(Session session) {
        this.session = session;
    }

    @Override
    public Parent findById(int id) {
        return session.get(Parent.class, id);
    }

    @Override
    public List<Parent> findAll() {
        return (List<Parent>) session.createQuery("FROM Parent").list();
    }

    @Override
    public void save(Parent parent) {
        Transaction tr = session.beginTransaction();
        session.save(parent);
        tr.commit();
    }

    @Override
    public void update(Parent parent) {
        Transaction tr = session.beginTransaction();
        session.update(parent);
        tr.commit();
    }

    @Override
    public void delete(Parent parent) {
        Transaction tr = session.beginTransaction();
        session.delete(parent);
        tr.commit();
    }
}
