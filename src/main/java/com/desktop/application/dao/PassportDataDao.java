package com.desktop.application.dao;

import com.desktop.application.entities.PassportData;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class PassportDataDao implements Dao<PassportData> {

    private final Session session;

    public PassportDataDao(Session session) {
        this.session = session;
    }

    @Override
    public PassportData findById(int id) {
        return session.get(PassportData.class, id);
    }

    @Override
    public List<PassportData> findAll() {
        return (List<PassportData>) session.createQuery("FROM PassportData").list();
    }

    @Override
    public void save(PassportData passportData) {
        Transaction tr = session.beginTransaction();
        session.save(passportData);
        tr.commit();
    }

    @Override
    public void update(PassportData passportData) {
        Transaction tr = session.beginTransaction();
        session.update(passportData);
        tr.commit();
    }

    @Override
    public void delete(PassportData passportData) {
        Transaction tr = session.beginTransaction();
        session.delete(passportData);
        tr.commit();
    }
}
