package com.desktop.application.dao;

import com.desktop.application.entities.Address;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class AddressDao implements Dao<Address> {

    private final Session session;

    public AddressDao(Session session) {
        this.session = session;
    }

    @Override
    public Address findById(int id) {
        return session.get(Address.class, id);
    }

    @Override
    public List<Address> findAll() {
        return (List<Address>) session.createQuery("FROM Address").list();
    }

    @Override
    public void save(Address address) {
        Transaction tr = session.beginTransaction();
        session.save(address);
        tr.commit();
    }

    @Override
    public void update(Address address) {
        Transaction tr = session.beginTransaction();
        session.update(address);
        tr.commit();
    }

    @Override
    public void delete(Address address) {
        Transaction tr = session.beginTransaction();
        session.delete(address);
        tr.commit();
    }
}
