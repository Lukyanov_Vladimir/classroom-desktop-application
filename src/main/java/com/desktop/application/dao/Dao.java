package com.desktop.application.dao;

import java.util.List;

public interface Dao<T> {
    T findById(int id);

    List<T> findAll();

    void save(T essence);

    void update(T essence);

    void delete(T essence);
}
