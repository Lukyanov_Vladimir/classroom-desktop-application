package com.desktop.application.controllers.addingRecordСontrollers;

import com.desktop.application.CreatingDate;
import com.desktop.application.TablesToDisplay;
import com.desktop.application.entities.Address;
import com.desktop.application.entities.BirthCertificate;
import com.desktop.application.entities.PassportData;
import com.desktop.application.entities.Student;
import com.desktop.application.services.StudentService;
import com.desktop.application.utils.HibernateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;

import java.util.Date;

/**
 * Класс контроллер для окна добавления студента
 */
public class AddingStudentController {

    @FXML
    private TextField fullNameStudent;

    @FXML
    private TextField dateBirthStudent;

    @FXML
    private TextField phoneNumberStudent;

    @FXML
    private TextField cityStudent;

    @FXML
    private TextField streetStudent;

    @FXML
    private TextField houseNumberStudent;

    @FXML
    private TextField flatNumberStudent;

    @FXML
    private TextField fullNamePassport;

    @FXML
    private TextField dateBirthPassport;

    @FXML
    private TextField cityPassport;

    @FXML
    private TextField streetPassport;

    @FXML
    private TextField houseNumPassport;

    @FXML
    private TextField flatNumPassport;

    @FXML
    private TextField seriesPassport;

    @FXML
    private TextField numberPassport;

    @FXML
    private TextField issuedByPassport;

    @FXML
    private TextField dateIssuePassport;

    @FXML
    private TextField departmentCodePassport;

    @FXML
    private TextField tin;

    @FXML
    private TextField snilsNumber;

    @FXML
    private TextField seriesBirthCertificate;

    @FXML
    private TextField numberBirthCertificate;

    @FXML
    private TextField issuedByBirthCertificate;

    @FXML
    private TextField dateIssueBirthCertificate;

    @FXML
    private Button saveBtn;

    private TablesToDisplay tablesToDisplay;

    /**
     * Метод устанавливает объект отображения таблиц
     *
     * @param tablesToDisplay - объект отображения таблиц
     */
    public void setTablesToDisplay(TablesToDisplay tablesToDisplay) {
        this.tablesToDisplay = tablesToDisplay;
    }

    @FXML
    public void initialize() {
        saveBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();

            StudentService studentService = new StudentService(session);
            studentService.save(createObjectStudent());

            session.close();

            showTableStudents();
            fieldsClearText();
        });
    }

    /**
     * Метод показвает таблицу "Студенты"
     */
    private void showTableStudents() {
        tablesToDisplay.showTableStudents();
    }

    /**
     * Метод создаёт объект студент
     *
     * @return - объект студент
     */
    private Student createObjectStudent() {
        Student student = new Student();
        student.setFullName(fullNameStudent.getText());
        student.setBirthDate(createObjectDate(dateBirthStudent.getText()));
        student.setPhoneNumber(parseLong(phoneNumberStudent.getText()));
        student.setAddress(createObjectAddress(
                cityStudent.getText(),
                streetStudent.getText(),
                parseInt(houseNumberStudent.getText()),
                parseInt(flatNumberStudent.getText()), student)
        );
        student.setPassportData(createObjectPassportData(student));
        student.setBirthCertificate(createObjectBirthCertificate(student));

        return student;
    }

    /**
     * Метод создаёт объект студент
     *
     * @param city - город
     * @param street - улица
     * @param houseNumber - номер дома
     * @param flatNumber - номер квартиры
     * @param student - студент
     * @return - адрес
     */
    private Address createObjectAddress(String city, String street, Integer houseNumber, Integer flatNumber, Student student) {
        Address address = new Address();
        address.setCity(city);
        address.setStreet(street);
        address.setHouseNumber(houseNumber);
        address.setFlatNumber(flatNumber);
        address.setStudent(student);

        return address;
    }

    /**
     * Метод создаёт объект студент
     *
     * @param city - город
     * @param street - улица
     * @param houseNumber - номер дома
     * @param flatNumber - номер квартиры
     * @param passportData - пасспортные данные
     * @return - адрес
     */
    private Address createObjectAddress(String city, String street, Integer houseNumber, Integer flatNumber, PassportData passportData) {
        Address address = new Address();
        address.setCity(city);
        address.setStreet(street);
        address.setHouseNumber(houseNumber);
        address.setFlatNumber(flatNumber);
        address.setPassportData(passportData);

        return address;
    }

    /**
     * Метод создаёт объект пасспортные данные
     *
     * @param student - студент
     * @return - пасспортные данные
     */
    private PassportData createObjectPassportData(Student student) {
        PassportData passportData = new PassportData();
        passportData.setFullName(fullNamePassport.getText());
        passportData.setDateBirth(createObjectDate(dateBirthPassport.getText()));
        passportData.setPlaceBirth(createObjectAddress(
                cityPassport.getText(),
                streetPassport.getText(),
                parseInt(houseNumPassport.getText()),
                parseInt(flatNumPassport.getText()),
                passportData)
        );
        passportData.setSeries(parseInt(seriesPassport.getText()));
        passportData.setNumber(parseInt(numberPassport.getText()));
        passportData.setIssuedBy(issuedByPassport.getText());
        passportData.setDateIssue(createObjectDate(dateIssuePassport.getText()));
        passportData.setDepartmentCode(parseLong(departmentCodePassport.getText()));
        passportData.setTin(parseLong(tin.getText()));
        passportData.setSnilsNumber(parseLong(snilsNumber.getText()));
        passportData.setStudent(student);

        return passportData;
    }

    /**
     * Метод создаёт объект свидительство о рождении
     *
     * @param student - студент
     * @return - свидительство о рождении
     */
    private BirthCertificate createObjectBirthCertificate(Student student) {
        BirthCertificate birthCertificate = new BirthCertificate();
        birthCertificate.setSeries(parseLong(seriesBirthCertificate.getText()));
        birthCertificate.setNumber(parseLong(numberBirthCertificate.getText()));
        birthCertificate.setIssuedBy(issuedByBirthCertificate.getText());
        birthCertificate.setDateIssue(createObjectDate(dateIssueBirthCertificate.getText()));
        birthCertificate.setStudent(student);

        return birthCertificate;
    }

    /**
     * Метод создаёт объект дата
     *
     * @param date - строка с датой
     * @return - дата
     */
    private Date createObjectDate(String date) {
        return CreatingDate.createDate(date);
    }

    /**
     * Метод преобразует строку в целое число
     *
     * @param str - строка
     * @return - целое число
     */
    private int parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Метод преобразует строку в десятичное число
     *
     * @param str - строка
     * @return - целое число
     */
    private long parseLong(String str) {
        return Long.parseLong(str);
    }

    /**
     * Метод очищает поля ввода данных
     */
    private void fieldsClearText() {
        clearField(fullNameStudent);
        clearField(dateBirthStudent);
        clearField(phoneNumberStudent);
        clearField(cityStudent);
        clearField(streetStudent);
        clearField(houseNumberStudent);
        clearField(flatNumberStudent);
        clearField(fullNamePassport);
        clearField(dateBirthPassport);
        clearField(cityPassport);
        clearField(streetPassport);
        clearField(houseNumPassport);
        clearField(flatNumPassport);
        clearField(seriesPassport);
        clearField(numberPassport);
        clearField(issuedByPassport);
        clearField(dateIssuePassport);
        clearField(departmentCodePassport);
        clearField(tin);
        clearField(snilsNumber);
        clearField(seriesBirthCertificate);
        clearField(numberBirthCertificate);
        clearField(issuedByBirthCertificate);
        clearField(dateIssueBirthCertificate);
    }

    /**
     * Метод очищает поле ввода данных
     *
     * @param textField - текстовое поле
     */
    private void clearField(TextField textField) {
        textField.clear();
    }
}
