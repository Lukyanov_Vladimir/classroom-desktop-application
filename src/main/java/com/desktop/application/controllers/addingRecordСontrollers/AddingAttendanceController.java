package com.desktop.application.controllers.addingRecordСontrollers;

import com.desktop.application.CreatingDate;
import com.desktop.application.TablesToDisplay;
import com.desktop.application.entities.Attendance;
import com.desktop.application.entities.Student;
import com.desktop.application.services.StudentService;
import com.desktop.application.utils.HibernateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;

import java.util.Date;

/**
 * Класс контроллер для окна добавления посещаемости
 */
public class AddingAttendanceController {

    @FXML
    private TextField numberHours;

    @FXML
    private TextField date;

    @FXML
    private TextField numberStudent;

    @FXML
    private Button saveBtn;

    private TablesToDisplay tablesToDisplay;

    /**
     * Метод устанавливает объект отображения таблиц
     *
     * @param tablesToDisplay - объект отображения таблиц
     */
    public void setTablesToDisplay(TablesToDisplay tablesToDisplay) {
        this.tablesToDisplay = tablesToDisplay;
    }

    @FXML
    public void initialize() {

        saveBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();

            StudentService studentService = new StudentService(session);

            Student student = studentService.find(parseInt(numberStudent.getText()));
            student.addAttendance(createObjectAttendance());

            studentService.update(student);

            session.close();

            showTableAttendances();
            fieldsClearText();
        });
    }

    /**
     * Метод показвает таблицу "Посещаемости"
     */
    private void showTableAttendances() {
        tablesToDisplay.showTableAttendances();
    }

    /**
     * Метод преобразует строку в целое число
     *
     * @param str - строка
     * @return - целое число
     */
    private int parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Метод создаёт объект посещаемость
     *
     * @return - объект посещаемость
     */
    private Attendance createObjectAttendance() {

        Attendance attendance = new Attendance();
        attendance.setNumberHours(parseInt(numberHours.getText()));
        attendance.setDate(createObjectDate(date.getText()));

        return attendance;
    }

    /**
     * Метод создаёт объект дата
     *
     * @param date - строка с датой
     * @return - дата
     */
    private Date createObjectDate(String date) {
        return CreatingDate.createDate(date);
    }

    /**
     * Метод очищает поля ввода данных
     */
    private void fieldsClearText() {
        clearField(numberHours);
        clearField(date);
        clearField(numberStudent);
    }

    /**
     * Метод очищает поле ввода данных
     *
     * @param textField - текстовое поле
     */
    private void clearField(TextField textField) {
        textField.clear();
    }
}
