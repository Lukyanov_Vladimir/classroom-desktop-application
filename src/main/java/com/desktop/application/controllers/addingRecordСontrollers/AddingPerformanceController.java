package com.desktop.application.controllers.addingRecordСontrollers;

import com.desktop.application.TablesToDisplay;
import com.desktop.application.entities.Discipline;
import com.desktop.application.entities.Performance;
import com.desktop.application.entities.Student;
import com.desktop.application.services.DisciplineService;
import com.desktop.application.services.StudentService;
import com.desktop.application.utils.HibernateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;

/**
 * Класс контроллер для окна добавления успеваемости
 */
public class AddingPerformanceController {

    @FXML
    private TextField mark;

    @FXML
    private TextField disciplineNumber;

    @FXML
    private TextField studentNumber;

    @FXML
    private Button saveBtn;

    private TablesToDisplay tablesToDisplay;

    /**
     * Метод устанавливает объект отображения таблиц
     *
     * @param tablesToDisplay - объект отображения таблиц
     */
    public void setTablesToDisplay(TablesToDisplay tablesToDisplay) {
        this.tablesToDisplay = tablesToDisplay;
    }

    @FXML
    public void initialize() {

        saveBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();

            StudentService studentService = new StudentService(session);

            Student student = studentService.find(parseInt(studentNumber.getText()));
            student.addPerformance(createObjectPerformance(session));

            studentService.update(student);

            session.close();

            showTablePerformances();
            fieldsClearText();
        });
    }

    /**
     * Метод показвает таблицу "Успеваемости"
     */
    private void showTablePerformances() {
        tablesToDisplay.showTablePerformances();
    }

    /**
     * Метод создаёт объект успеваемость
     *
     * @return - объект успеваемость
     */
    private Performance createObjectPerformance(Session session) {
        Performance performance = new Performance();
        performance.setMark(parseInt(mark.getText()));
        DisciplineService disciplineService = new DisciplineService(session);
        Discipline discipline = disciplineService.find(parseInt(disciplineNumber.getText()));
        discipline.addPerformance(performance);

        return performance;
    }

    /**
     * Метод преобразует строку в целое число
     *
     * @param str - строка
     * @return - целое число
     */
    private Integer parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Метод очищает поля ввода данных
     */
    private void fieldsClearText() {
        clearField(mark);
        clearField(disciplineNumber);
        clearField(studentNumber);
    }

    /**
     * Метод очищает поле ввода данных
     *
     * @param textField - текстовое поле
     */
    private void clearField(TextField textField) {
        textField.clear();
    }
}
