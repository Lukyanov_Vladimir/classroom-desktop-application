package com.desktop.application.controllers.addingRecordСontrollers;

import com.desktop.application.TablesToDisplay;
import com.desktop.application.entities.Address;
import com.desktop.application.entities.Parent;
import com.desktop.application.entities.Student;
import com.desktop.application.services.StudentService;
import com.desktop.application.utils.HibernateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;

/**
 * Класс контроллер для окна добавления родителя
 */
public class AddingParentController {

    @FXML
    private TextField fullName;

    @FXML
    private TextField familyStatus;

    @FXML
    private TextField numberChildren;

    @FXML
    private TextField phoneNumber;

    @FXML
    private TextField placeWork;

    @FXML
    private TextField studentNumber;

    @FXML
    private TextField city;

    @FXML
    private TextField street;

    @FXML
    private TextField houseNumber;

    @FXML
    private TextField flatNumber;

    @FXML
    private Button saveBtn;

    private TablesToDisplay tablesToDisplay;

    /**
     * Метод устанавливает объект отображения таблиц
     *
     * @param tablesToDisplay - объект отображения таблиц
     */
    public void setTablesToDisplay(TablesToDisplay tablesToDisplay) {
        this.tablesToDisplay = tablesToDisplay;
    }

    @FXML
    public void initialize() {

        saveBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();

            StudentService studentService = new StudentService(session);

            Student student = studentService.find(parseInt(studentNumber.getText()));
            student.addParent(createObjectParent());

            studentService.update(student);

            session.close();

            showTableParents();
            fieldsClearText();
        });
    }

    /**
     * Метод показвает таблицу "Родители"
     */
    private void showTableParents() {
        tablesToDisplay.showTableParents();
    }


    /**
     * Метод создаёт объект родитель
     *
     * @return - объект родитель
     */
    private Parent createObjectParent() {
        Parent parent = new Parent();
        parent.setFullName(fullName.getText());
        parent.setFamilyStatus(familyStatus.getText());
        parent.setNumberChildren(parseInt(numberChildren.getText()));
        parent.setPhoneNumber(parseLong(phoneNumber.getText()));
        parent.setPlaceWork(placeWork.getText());
        parent.setAddress(createObjectAddress(parent));

        return parent;
    }

    /**
     * Метод создаёт объект адрес
     *
     * @param parent - родитель
     * @return - адрес
     */
    private Address createObjectAddress(Parent parent) {
        Address address = new Address();
        address.setCity(city.getText());
        address.setStreet(street.getText());
        address.setHouseNumber(parseInt(houseNumber.getText()));
        address.setFlatNumber(parseInt(flatNumber.getText()));
        address.setParent(parent);

        return address;
    }

    /**
     * Метод преобразует строку в целое число
     *
     * @param str - строка
     * @return - целое число
     */
    private int parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Метод преобразует строку в десятичное число
     *
     * @param str - строка
     * @return - целое число
     */
    private long parseLong(String str) {
        return Long.parseLong(str);
    }

    /**
     * Метод очищает поля ввода данных
     */
    private void fieldsClearText() {
        clearField(fullName);
        clearField(familyStatus);
        clearField(numberChildren);
        clearField(phoneNumber);
        clearField(placeWork);
        clearField(studentNumber);
        clearField(city);
        clearField(street);
        clearField(houseNumber);
        clearField(flatNumber);
    }

    /**
     * Метод очищает поле ввода данных
     *
     * @param textField - текстовое поле
     */
    private void clearField(TextField textField) {
        textField.clear();
    }
}
