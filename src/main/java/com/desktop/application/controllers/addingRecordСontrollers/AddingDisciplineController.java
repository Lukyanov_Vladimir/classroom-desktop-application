package com.desktop.application.controllers.addingRecordСontrollers;

import com.desktop.application.TablesToDisplay;
import com.desktop.application.entities.Discipline;
import com.desktop.application.services.DisciplineService;
import com.desktop.application.utils.HibernateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;

/**
 * Класс контроллер для окна добавления дисциплины
 */
public class AddingDisciplineController {

    @FXML
    private TextField disciplineName;

    @FXML
    private TextField teacherFullName;

    @FXML
    private Button saveBtn;

    private TablesToDisplay tablesToDisplay;

    /**
     * Метод устанавливает объект отображения таблиц
     *
     * @param tablesToDisplay - объект отображения таблиц
     */
    public void setTablesToDisplay(TablesToDisplay tablesToDisplay) {
        this.tablesToDisplay = tablesToDisplay;
    }

    @FXML
    public void initialize() {

        saveBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();

            DisciplineService disciplineService = new DisciplineService(session);
            disciplineService.save(createObjectDiscipline());

            session.close();

            showTableDisciplines();
            fieldsClearText();
        });
    }

    /**
     * Метод создаёт объект дисциплины
     *
     * @return - объект дисциплины
     */
    private Discipline createObjectDiscipline() {
        Discipline discipline = new Discipline();
        discipline.setDisciplineName(disciplineName.getText());
        discipline.setFullNameTeacher(teacherFullName.getText());

        return discipline;
    }

    /**
     * Метод показвает таблицу "Дисциплины"
     */
    private void showTableDisciplines() {
        tablesToDisplay.showTableDisciplines();
    }

    /**
     * Метод очищает поля ввода данных
     */
    private void fieldsClearText() {
        clearField(disciplineName);
        clearField(teacherFullName);
    }

    /**
     * Метод очищает поле ввода данных
     *
     * @param textField - текстовое поле
     */
    private void clearField(TextField textField) {
        textField.clear();
    }
}
