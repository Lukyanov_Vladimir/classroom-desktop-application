package com.desktop.application.controllers.editingRecordControllers;

import com.desktop.application.TablesToDisplay;
import com.desktop.application.entities.Address;
import com.desktop.application.entities.Parent;
import com.desktop.application.services.ParentService;
import com.desktop.application.utils.HibernateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;

/**
 * Класс контроллер для окна редактирования родителя
 */
public class EditingParentController {

    @FXML
    private TextField parentNumber;

    @FXML
    private Button okBtn;

    @FXML
    private TextField fullName;

    @FXML
    private TextField familyStatus;

    @FXML
    private TextField numberChildren;

    @FXML
    private TextField phoneNumber;

    @FXML
    private TextField placeWork;

    @FXML
    private TextField city;

    @FXML
    private TextField street;

    @FXML
    private TextField houseNumber;

    @FXML
    private TextField flatNumber;

    @FXML
    private Button saveBtn;

    private TablesToDisplay tablesToDisplay;

    /**
     * Метод устанавливает объект отображения таблиц
     *
     * @param tablesToDisplay - объект отображения таблиц
     */
    public void setTablesToDisplay(TablesToDisplay tablesToDisplay) {
        this.tablesToDisplay = tablesToDisplay;
    }

    @FXML
    public void initialize() {

        okBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();

            ParentService parentService = new ParentService(session);
            Parent parent = parentService.find(parseInt(parentNumber.getText()));

            session.close();

            fillFields(parent);
        });

        saveBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();

            ParentService parentService = new ParentService(session);
            Parent parent = parentService.find(parseInt(parentNumber.getText()));

            parentService.update(changeObjectParent(parent));

            session.close();

            showTableParents();
            fieldsClearText();
        });
    }

    /**
     * Метод показвает таблицу "Родители"
     */
    private void showTableParents() {
        tablesToDisplay.showTableParents();
    }

    /**
     * Метод создаёт объект родитель
     *
     * @param parent - родитель
     * @return - объект родитель
     */
    private Parent changeObjectParent(Parent parent) {
        parent.setFullName(fullName.getText());
        parent.setFamilyStatus(familyStatus.getText());
        parent.setNumberChildren(parseInt(numberChildren.getText()));
        parent.setPhoneNumber(parseLong(phoneNumber.getText()));
        parent.setPlaceWork(placeWork.getText());
        parent.setAddress(createObjectAddress(parent));

        return parent;
    }

    /**
     * Метод создаёт объект адрес
     *
     * @param parent - родитель
     * @return - адрес
     */
    private Address createObjectAddress(Parent parent) {
        Address address = new Address();
        address.setCity(city.getText());
        address.setStreet(street.getText());
        address.setHouseNumber(parseInt(houseNumber.getText()));
        address.setFlatNumber(parseInt(flatNumber.getText()));
        address.setParent(parent);

        return address;
    }

    /**
     * Метод преобразует строку в целое число
     *
     * @param str - строка
     * @return - целое число
     */
    private int parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Метод преобразует строку в десятичное число
     *
     * @param str - строка
     * @return - целое число
     */
    private long parseLong(String str) {
        return Long.parseLong(str);
    }

    /**
     * Метод заполняет поля данными
     *
     * @param parent - родитель
     */
    private void fillFields(Parent parent) {
        fillField(fullName, parent.getFullName());
        fillField(familyStatus, parent.getFamilyStatus());
        fillField(numberChildren, parent.getNumberChildren().toString());
        fillField(phoneNumber, parent.getPhoneNumber().toString());
        fillField(placeWork, parent.getPlaceWork());
        fillField(city, parent.getAddress().getCity());
        fillField(street, parent.getAddress().getStreet());
        fillField(houseNumber, parent.getAddress().getHouseNumber().toString());
        fillField(flatNumber, parent.getAddress().getFlatNumber().toString());
    }

    /**
     * Метод очищает поля ввода данных
     */
    private void fieldsClearText() {
        clearField(parentNumber);
        clearField(fullName);
        clearField(familyStatus);
        clearField(numberChildren);
        clearField(phoneNumber);
        clearField(placeWork);
        clearField(city);
        clearField(street);
        clearField(houseNumber);
        clearField(flatNumber);
    }

    /**
     * Метод заполняет поле данными
     *
     * @param textField - текстовое поле
     * @param text      - текст
     */
    private void fillField(TextField textField, String text) {
        textField.setText(text);
    }

    /**
     * Метод очищает поле ввода данных
     *
     * @param textField - текстовое поле
     */
    private void clearField(TextField textField) {
        textField.clear();
    }
}
