package com.desktop.application.controllers.editingRecordControllers;

import com.desktop.application.TablesToDisplay;
import com.desktop.application.entities.Discipline;
import com.desktop.application.services.DisciplineService;
import com.desktop.application.utils.HibernateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;

/**
 * Класс контроллер для окна редактирования дисциплины
 */
public class EditingDisciplineController {

    @FXML
    private TextField disciplineNumber;

    @FXML
    private Button okBtn;

    @FXML
    private TextField disciplineName;

    @FXML
    private TextField teacherFullName;

    @FXML
    private Button saveBtn;

    private TablesToDisplay tablesToDisplay;

    /**
     * Метод устанавливает объект отображения таблиц
     *
     * @param tablesToDisplay - объект отображения таблиц
     */
    public void setTablesToDisplay(TablesToDisplay tablesToDisplay) {
        this.tablesToDisplay = tablesToDisplay;
    }

    @FXML
    public void initialize() {

        okBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();

            DisciplineService disciplineService = new DisciplineService(session);
            Discipline discipline = disciplineService.find(parseInt(disciplineNumber.getText()));

            session.close();

            fillFields(discipline);
        });

        saveBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();

            DisciplineService disciplineService = new DisciplineService(session);
            Discipline discipline = disciplineService.find(parseInt(disciplineNumber.getText()));
            disciplineService.update(changeObjectDiscipline(discipline));

            session.close();

            showTableDisciplines();
            fieldsClearText();
        });
    }

    /**
     * Метод изменяет объект дисциплины
     *
     * @param discipline - дисциплина
     * @return - объект дисциплины
     */
    private Discipline changeObjectDiscipline(Discipline discipline) {
        discipline.setDisciplineName(disciplineName.getText());
        discipline.setFullNameTeacher(teacherFullName.getText());

        return discipline;
    }

    /**
     * Метод показвает таблицу "Дисциплины"
     */
    private void showTableDisciplines() {
        tablesToDisplay.showTableDisciplines();
    }

    /**
     * Метод заполняет поля данными
     *
     * @param discipline - дисциплина
     */
    private void fillFields(Discipline discipline) {
        fillField(disciplineName, discipline.getDisciplineName());
        fillField(teacherFullName, discipline.getFullNameTeacher());
    }

    /**
     * Метод очищает поля ввода данных
     */
    private void fieldsClearText() {
        clearField(disciplineNumber);
        clearField(disciplineName);
        clearField(teacherFullName);
    }

    /**
     * Метод заполняет поле данными
     *
     * @param textField - текстовое поле
     * @param text      - текст
     */
    private void fillField(TextField textField, String text) {
        textField.setText(text);
    }

    /**
     * Метод очищает поле ввода данных
     *
     * @param textField - текстовое поле
     */
    private void clearField(TextField textField) {
        textField.clear();
    }

    /**
     * Метод создаёт объект посещаемость
     *
     * @return - объект посещаемость
     */
    private int parseInt(String str) {
        return Integer.parseInt(str);
    }
}
