package com.desktop.application.controllers.editingRecordControllers;

import com.desktop.application.TablesToDisplay;
import com.desktop.application.entities.Performance;
import com.desktop.application.services.DisciplineService;
import com.desktop.application.services.PerformanceService;
import com.desktop.application.utils.HibernateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;

/**
 * Класс контроллер для окна редактирования успеваемости
 */
public class EditingPerformanceController {

    @FXML
    private TextField performanceNumber;

    @FXML
    private Button okBtn;

    @FXML
    private TextField mark;

    @FXML
    private TextField disciplineNumber;

    @FXML
    private Button saveBtn;

    private TablesToDisplay tablesToDisplay;

    /**
     * Метод устанавливает объект отображения таблиц
     *
     * @param tablesToDisplay - объект отображения таблиц
     */
    public void setTablesToDisplay(TablesToDisplay tablesToDisplay) {
        this.tablesToDisplay = tablesToDisplay;
    }

    @FXML
    public void initialize() {

        okBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();

            PerformanceService performanceService = new PerformanceService(session);
            Performance performance = performanceService.find(parseInt(performanceNumber.getText()));

            session.close();

            fillFields(performance);
        });

        saveBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();
            PerformanceService performanceService = new PerformanceService(session);
            Performance performance = performanceService.find(parseInt(performanceNumber.getText()));

            performanceService.update(changeObjectPerformance(performance, session));

            session.close();

            showTablePerformances();
            fieldsClearText();
        });
    }

    /**
     * Метод показвает таблицу "Успеваемости"
     */
    private void showTablePerformances() {
        tablesToDisplay.showTablePerformances();
    }

    /**
     * Метод создаёт объект успеваемость
     *
     * @param performance - успеваемость
     * @return - объект успеваемость
     */
    private Performance changeObjectPerformance(Performance performance, Session session) {
        performance.setMark(parseInt(mark.getText()));
        DisciplineService disciplineService = new DisciplineService(session);
        performance.setDiscipline(disciplineService.find(parseInt(disciplineNumber.getText())));

        return performance;
    }

    /**
     * Метод преобразует строку в целое число
     *
     * @param str - строка
     * @return - целое число
     */
    private int parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Метод заполняет поля данными
     *
     * @param performance - успеваемость
     */
    private void fillFields(Performance performance) {
        fillField(mark, performance.getMark().toString());
        fillField(disciplineNumber, performance.getDiscipline().getId().toString());
    }

    /**
     * Метод очищает поля ввода данных
     */
    private void fieldsClearText() {
        clearField(performanceNumber);
        clearField(mark);
        clearField(disciplineNumber);
    }

    /**
     * Метод заполняет поле данными
     *
     * @param textField - текстовое поле
     * @param text      - текст
     */
    private void fillField(TextField textField, String text) {
        textField.setText(text);
    }

    /**
     * Метод очищает поле ввода данных
     *
     * @param textField - текстовое поле
     */
    private void clearField(TextField textField) {
        textField.clear();
    }
}
