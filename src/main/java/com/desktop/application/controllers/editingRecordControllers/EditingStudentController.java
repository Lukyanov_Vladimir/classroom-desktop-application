package com.desktop.application.controllers.editingRecordControllers;

import com.desktop.application.CreatingDate;
import com.desktop.application.TablesToDisplay;
import com.desktop.application.entities.Address;
import com.desktop.application.entities.BirthCertificate;
import com.desktop.application.entities.PassportData;
import com.desktop.application.entities.Student;
import com.desktop.application.services.StudentService;
import com.desktop.application.utils.HibernateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;

import java.util.Date;

/**
 * Класс контроллер для окна редактирования студента
 */
public class EditingStudentController {

    @FXML
    private TextField studentNumber;

    @FXML
    private Button okBtn;

    @FXML
    private TextField fullNameStudent;

    @FXML
    private TextField dateBirthStudent;

    @FXML
    private TextField phoneNumberStudent;

    @FXML
    private TextField cityStudent;

    @FXML
    private TextField streetStudent;

    @FXML
    private TextField houseNumberStudent;

    @FXML
    private TextField flatNumberStudent;

    @FXML
    private TextField fullNamePassport;

    @FXML
    private TextField dateBirthPassport;

    @FXML
    private TextField cityPassport;

    @FXML
    private TextField streetPassport;

    @FXML
    private TextField houseNumPassport;

    @FXML
    private TextField flatNumPassport;

    @FXML
    private TextField seriesPassport;

    @FXML
    private TextField numberPassport;

    @FXML
    private TextField issuedByPassport;

    @FXML
    private TextField dateIssuePassport;

    @FXML
    private TextField departmentCodePassport;

    @FXML
    private TextField tin;

    @FXML
    private TextField snilsNumber;

    @FXML
    private TextField seriesBirthCertificate;

    @FXML
    private TextField numberBirthCertificate;

    @FXML
    private TextField issuedByBirthCertificate;

    @FXML
    private TextField dateIssueBirthCertificate;

    @FXML
    private Button changeBtn;

    private TablesToDisplay tablesToDisplay;

    /**
     * Метод устанавливает объект отображения таблиц
     *
     * @param tablesToDisplay - объект отображения таблиц
     */
    public void setTablesToDisplay(TablesToDisplay tablesToDisplay) {
        this.tablesToDisplay = tablesToDisplay;
    }

    @FXML
    public void initialize() {

        okBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();
            StudentService studentService = new StudentService(session);
            Student student = studentService.find(parseInt(studentNumber.getText()));
            session.close();
            fillFields(student);
        });

        changeBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();
            StudentService studentService = new StudentService(session);
            Student student = studentService.find(parseInt(studentNumber.getText()));
            studentService.update(ChangeObjectStudent(student));
            session.close();
            showTableStudents();
            fieldsClearText();
        });
    }

    /**
     * Метод показвает таблицу "Студенты"
     */
    private void showTableStudents() {
        tablesToDisplay.showTableStudents();
    }

    /**
     * Метод создаёт объект студент
     *
     * @param student - студент
     * @return - объект студент
     */
    private Student ChangeObjectStudent(Student student) {
        student.setFullName(fullNameStudent.getText());
        student.setBirthDate(createObjectDate(dateBirthStudent.getText()));
        student.setPhoneNumber(parseLong(phoneNumberStudent.getText()));
        student.setAddress(createObjectAddress(cityStudent.getText(), streetStudent.getText(), parseInt(houseNumberStudent.getText()),
                parseInt(flatNumberStudent.getText()), student));
        student.setPassportData(createObjectPassportData(student));
        student.setBirthCertificate(createObjectBirthCertificate(student));

        return student;
    }

    /**
     * Метод создаёт объект студент
     *
     * @param city        - город
     * @param street      - улица
     * @param houseNumber - номер дома
     * @param flatNumber  - номер квартиры
     * @param student     - студент
     * @return - адрес
     */
    private Address createObjectAddress(String city, String street, Integer houseNumber, Integer flatNumber, Student student) {
        Address address = new Address();
        address.setCity(city);
        address.setStreet(street);
        address.setHouseNumber(houseNumber);
        address.setFlatNumber(flatNumber);
        address.setStudent(student);

        return address;
    }

    /**
     * Метод создаёт объект студент
     *
     * @param city         - город
     * @param street       - улица
     * @param houseNumber  - номер дома
     * @param flatNumber   - номер квартиры
     * @param passportData - пасспортные данные
     * @return - адрес
     */
    private Address createObjectAddress(String city, String street, Integer houseNumber, Integer flatNumber, PassportData passportData) {
        Address address = new Address();
        address.setCity(city);
        address.setStreet(street);
        address.setHouseNumber(houseNumber);
        address.setFlatNumber(flatNumber);
        address.setPassportData(passportData);

        return address;
    }

    /**
     * Метод создаёт объект пасспортные данные
     *
     * @param student - студент
     * @return - пасспортные данные
     */
    private PassportData createObjectPassportData(Student student) {
        PassportData passportData = new PassportData();
        passportData.setFullName(fullNamePassport.getText());
        passportData.setDateBirth(createObjectDate(dateBirthPassport.getText()));
        passportData.setPlaceBirth(createObjectAddress(cityPassport.getText(),
                streetPassport.getText(),
                parseInt(houseNumPassport.getText()),
                parseInt(flatNumPassport.getText()),
                passportData)
        );
        passportData.setSeries(parseInt(seriesPassport.getText()));
        passportData.setNumber(parseInt(numberPassport.getText()));
        passportData.setIssuedBy(issuedByPassport.getText());
        passportData.setDateIssue(createObjectDate(dateIssuePassport.getText()));
        passportData.setDepartmentCode(parseLong(departmentCodePassport.getText()));
        passportData.setTin(parseLong(tin.getText()));
        passportData.setSnilsNumber(parseLong(snilsNumber.getText()));
        passportData.setStudent(student);

        return passportData;
    }

    /**
     * Метод создаёт объект свидительство о рождении
     *
     * @param student - студент
     * @return - свидительство о рождении
     */
    private BirthCertificate createObjectBirthCertificate(Student student) {
        BirthCertificate birthCertificate = new BirthCertificate();
        birthCertificate.setSeries(parseLong(seriesBirthCertificate.getText()));
        birthCertificate.setNumber(parseLong(numberBirthCertificate.getText()));
        birthCertificate.setIssuedBy(issuedByBirthCertificate.getText());
        birthCertificate.setDateIssue(createObjectDate(dateIssueBirthCertificate.getText()));
        birthCertificate.setStudent(student);

        return birthCertificate;
    }

    /**
     * Метод создаёт объект дата
     *
     * @param date - строка с датой
     * @return - дата
     */
    private Date createObjectDate(String date) {
        return CreatingDate.createDate(date);
    }

    /**
     * Метод преобразует строку в целое число
     *
     * @param str - строка
     * @return - целое число
     */
    private int parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Метод преобразует строку в десятичное число
     *
     * @param str - строка
     * @return - целое число
     */
    private long parseLong(String str) {
        return Long.parseLong(str);
    }

    /**
     * Метод заполняет поля данными
     *
     * @param student - студент
     */
    private void fillFields(Student student) {
        fillField(fullNameStudent, student.getFullName());
        fillField(dateBirthStudent, student.getBirthDate().toString());
        fillField(phoneNumberStudent, student.getPhoneNumber().toString());
        fillField(cityStudent, student.getAddress().getCity());
        fillField(streetStudent, student.getAddress().getStreet());
        fillField(houseNumberStudent, student.getAddress().getHouseNumber().toString());
        fillField(flatNumberStudent, student.getAddress().getFlatNumber().toString());
        fillField(fullNamePassport, student.getPassportData().getFullName());
        fillField(dateBirthPassport, student.getPassportData().getDateBirth().toString());
        fillField(cityPassport, student.getPassportData().getPlaceBirth().getCity());
        fillField(streetPassport, student.getPassportData().getPlaceBirth().getStreet());
        fillField(houseNumPassport, student.getPassportData().getPlaceBirth().getHouseNumber().toString());
        fillField(flatNumPassport, student.getPassportData().getPlaceBirth().getFlatNumber().toString());
        fillField(seriesPassport, student.getPassportData().getSeries().toString());
        fillField(numberPassport, student.getPassportData().getNumber().toString());
        fillField(issuedByPassport, student.getPassportData().getIssuedBy());
        fillField(dateIssuePassport, student.getPassportData().getDateIssue().toString());
        fillField(departmentCodePassport, student.getPassportData().getDepartmentCode().toString());
        fillField(tin, student.getPassportData().getTin().toString());
        fillField(snilsNumber, student.getPassportData().getSnilsNumber().toString());
        fillField(seriesBirthCertificate, student.getBirthCertificate().getSeries().toString());
        fillField(numberBirthCertificate, student.getBirthCertificate().getNumber().toString());
        fillField(issuedByBirthCertificate, student.getBirthCertificate().getIssuedBy());
        fillField(dateIssueBirthCertificate, student.getBirthCertificate().getDateIssue().toString());
    }

    /**
     * Метод очищает поля ввода данных
     */
    private void fieldsClearText() {
        clearField(studentNumber);
        clearField(fullNameStudent);
        clearField(dateBirthStudent);
        clearField(phoneNumberStudent);
        clearField(cityStudent);
        clearField(streetStudent);
        clearField(houseNumberStudent);
        clearField(flatNumberStudent);
        clearField(fullNamePassport);
        clearField(dateBirthPassport);
        clearField(cityPassport);
        clearField(streetPassport);
        clearField(houseNumPassport);
        clearField(flatNumPassport);
        clearField(seriesPassport);
        clearField(numberPassport);
        clearField(issuedByPassport);
        clearField(dateIssuePassport);
        clearField(departmentCodePassport);
        clearField(tin);
        clearField(snilsNumber);
        clearField(seriesBirthCertificate);
        clearField(numberBirthCertificate);
        clearField(issuedByBirthCertificate);
        clearField(dateIssueBirthCertificate);
    }

    /**
     * Метод заполняет поле данными
     *
     * @param textField - текстовое поле
     * @param text      - текст
     */
    private void fillField(TextField textField, String text) {
        textField.setText(text);
    }

    /**
     * Метод очищает поле ввода данных
     *
     * @param textField - текстовое поле
     */
    private void clearField(TextField textField) {
        textField.clear();
    }
}
