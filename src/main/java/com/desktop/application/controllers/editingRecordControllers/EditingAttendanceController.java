package com.desktop.application.controllers.editingRecordControllers;

import com.desktop.application.CreatingDate;
import com.desktop.application.TablesToDisplay;
import com.desktop.application.entities.Attendance;
import com.desktop.application.services.AttendanceService;
import com.desktop.application.utils.HibernateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;

import java.util.Date;

/**
 * Класс контроллер для окна редактирования посещаемости
 */
public class EditingAttendanceController {

    @FXML
    private TextField attendanceNumber;

    @FXML
    private Button okBtn;

    @FXML
    private TextField numberHours;

    @FXML
    private TextField date;

    @FXML
    private Button saveBtn;

    private TablesToDisplay tablesToDisplay;

    /**
     * Метод устанавливает объект отображения таблиц
     *
     * @param tablesToDisplay - объект отображения таблиц
     */
    public void setTablesToDisplay(TablesToDisplay tablesToDisplay) {
        this.tablesToDisplay = tablesToDisplay;
    }

    public void initialize() {

        okBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();

            AttendanceService attendanceService = new AttendanceService(session);
            Attendance attendance = attendanceService.find(parseInt(attendanceNumber.getText()));

            session.close();

            fillFields(attendance);
        });

        saveBtn.setOnAction(event -> {
            Session session = HibernateUtil.openSession();

            AttendanceService attendanceService = new AttendanceService(session);

            Attendance attendance = attendanceService.find(parseInt(attendanceNumber.getText()));
            attendanceService.update(changeObjectAttendance(attendance));

            session.close();

            showTableAttendances();
            fieldsClearText();
        });
    }

    /**
     * Метод показвает таблицу "Посещаемости"
     */
    private void showTableAttendances() {
        tablesToDisplay.showTableAttendances();
    }

    /**
     * Метод преобразует строку в целое число
     *
     * @param str - строка
     * @return - целое число
     */
    private int parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Метод изменяет объект посещаемость
     *
     * @param attendance - посещаемость
     * @return - объект посещаемость
     */
    private Attendance changeObjectAttendance(Attendance attendance) {
        attendance.setNumberHours(parseInt(numberHours.getText()));
        attendance.setDate(createObjectDate(date.getText()));

        return attendance;
    }

    /**
     * Метод создаёт объект дата
     *
     * @param date - строка с датой
     * @return - дата
     */
    private Date createObjectDate(String date) {
        return CreatingDate.createDate(date);
    }

    /**
     * Метод заполняет поля данными
     *
     * @param attendance - посещаемость
     */
    private void fillFields(Attendance attendance) {
        fillField(numberHours, attendance.getNumberHours().toString());
        fillField(date, attendance.getDate().toString());
    }


    /**
     * Метод очищает поля ввода данных
     */
    private void fieldsClearText() {
        clearField(attendanceNumber);
        clearField(numberHours);
        clearField(date);
    }

    /**
     * Метод заполняет поле данными
     *
     * @param textField - текстовое поле
     * @param text      - текст
     */
    private void fillField(TextField textField, String text) {
        textField.setText(text);
    }

    /**
     * Метод очищает поле ввода данных
     *
     * @param textField - текстовое поле
     */
    private void clearField(TextField textField) {
        textField.clear();
    }
}
