package com.desktop.application.controllers.mainController;

import com.desktop.application.Tables;
import com.desktop.application.TablesToDisplay;
import com.desktop.application.Windows;
import com.desktop.application.entities.*;
import com.desktop.application.utils.HibernateUtil;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Класс контроллер главного окна
 */
public class MainController {

    @FXML
    private TableView<Student> mainTable;

    @FXML
    private TableView<Performance> tablePerformances;
    @FXML
    private TableView<Parent> tableParents;

    @FXML
    private TableView<Attendance> tableAttendances;

    @FXML
    private TableView<Discipline> tableDisciplines;

    @FXML
    private GridPane mainMenu;

    @FXML
    private HBox studentsBtn;

    @FXML
    private HBox passportsDataBtn;

    @FXML
    private HBox performancesBtn;

    @FXML
    private HBox attendancesBtn;

    @FXML
    private HBox birthCertificatesBtn;

    @FXML
    private HBox parentsBtn;

    @FXML
    private HBox disciplinesBtn;

    @FXML
    private HBox editModeBtn;

    @FXML
    private HBox exitBtn;

    @FXML
    private GridPane editorMenu;

    @FXML
    private HBox addRecordBtn;

    @FXML
    private HBox removeRecordBtn;

    @FXML
    private HBox editRecordBtn;

    @FXML
    private HBox exitEditModeBtn;

    private Stage primaryStage;

    private TablesToDisplay tablesToDisplay;

    //Обработчик нажатий кнопки "Студенты"
    private final EventHandler<MouseEvent> studentsBtnHandler = event -> {
        resetDisableButtons();
        disableBtn(studentsBtn);
        tablesToDisplay.showTableStudents();
    };

    //Обработчик нажатий кнопки "Пасспортные данные"
    private final EventHandler<MouseEvent> passportsDataBtnHandler = event -> {
        resetDisableButtons();
        disableBtn(editModeBtn);
        disableBtn(passportsDataBtn);
        tablesToDisplay.showTablePassportsData();
    };

    //Обработчик нажатий кнопки "Успеваемости"
    private final EventHandler<MouseEvent> performancesBtnHandler = event -> {
        resetDisableButtons();
        disableBtn(performancesBtn);
        tablesToDisplay.showTablePerformances();
    };

    //Обработчик нажатий кнопки "Посещаемости"
    private final EventHandler<MouseEvent> attendancesBtnHandler = event -> {
        resetDisableButtons();
        disableBtn(attendancesBtn);
        tablesToDisplay.showTableAttendances();
    };

    //Обработчик нажатий кнопки "Свидительства о рождении"
    private final EventHandler<MouseEvent> birthCertificatesBtnHandler = event -> {
        resetDisableButtons();
        disableBtn(editModeBtn);
        disableBtn(birthCertificatesBtn);
        tablesToDisplay.showTableBirthCertificates();
    };

    //Обработчик нажатий кнопки "Родители"
    private final EventHandler<MouseEvent> parentsBtnHandler = event -> {
        resetDisableButtons();
        disableBtn(parentsBtn);
        tablesToDisplay.showTableParents();
    };

    //Обработчик нажатий кнопки "Дисциплины"
    private final EventHandler<MouseEvent> disciplinesBtnHandler = event -> {
        resetDisableButtons();
        disableBtn(disciplinesBtn);
        tablesToDisplay.showTableDisciplines();
    };

    //Обработчик нажатий кнопки "Выход"
    private final EventHandler<MouseEvent> exitBtnHandler = event -> shutdown();

    //Обработчик нажатий кнопки "Режим редактирования"
    private final EventHandler<MouseEvent> editorModeBtnHandler = event -> {
        try {
            switchMenu();
        } catch (NullPointerException e) {
            e.printStackTrace();
            Windows.openInformationWindow(Alert.AlertType.WARNING, "Ошибка", null, "Таблица не выбрана!");
        }
    };

    //Обработчик нажатий кнопки "Добавить запись"
    private final EventHandler<MouseEvent> addRecordBtnHandler = event -> {
        try {
            openTableAddingWindow();
        } catch (IOException e) {
            e.printStackTrace();
        }
    };

    //Обработчик нажатий кнопки "Удалить запись"
    private final EventHandler<MouseEvent> removeRecordBtnHandler = event -> {
        try {
            openTableRemovalWindow();
        } catch (IOException e) {
            e.printStackTrace();
        }
    };

    //Обработчик нажатий кнопки "Редактировать запись"
    private final EventHandler<MouseEvent> editRecordBtnHandler = event -> {
        try {
            openTableEditingWindow();
        } catch (IOException e) {
            e.printStackTrace();
        }
    };

    //Обработчик нажатий кнопки "Выход из режима редактирования"
    private final EventHandler<MouseEvent> exitEditorModeBtnHandler = event -> {
        resetActivityEditButtons();
        switchMenus(false, true);
    };

    /**
     * Метод устанавливает главное окно
     *
     * @param primaryStage - главное окно
     */
    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @FXML
    public void initialize() {
        startHibernate();
        tablesToDisplay = new TablesToDisplay(mainTable, tablePerformances, tableParents, tableAttendances, tableDisciplines);
        addHandlersAllButtons();
    }

    /**
     * Метод завершения работы программы
     */
    public void shutdown() {
        shutdownHibernate();
        closeApp();
    }

    /**
     * Метод закрытия главного окна программы
     */
    private void closeApp() {
        primaryStage.close();
    }

    /**
     * Метод стартует hibernate
     */
    private void startHibernate() {
        HibernateUtil.buildSessionFactory();
    }

    /**
     * Метод останавливает hibernate
     */
    private void shutdownHibernate() {
        HibernateUtil.shutdown();
    }

    /**
     * Метод изменяет активноть кнопок редактировния
     *
     * @param addRecordBtn - кнопка добавления записи
     * @param removeRecordBtn - кнопка удаления записи
     * @param editRecordBtn - кнопка добавления записи
     */
    private void changeActivityEditButtons(Boolean addRecordBtn, Boolean removeRecordBtn, Boolean editRecordBtn) {
        this.addRecordBtn.setDisable(addRecordBtn);
        this.removeRecordBtn.setDisable(removeRecordBtn);
        this.editRecordBtn.setDisable(editRecordBtn);
    }

    /**
     * Метод возвращает активность кнопок в исходное состояние
     */
    private void resetActivityEditButtons() {
        enableButton(addRecordBtn);
        enableButton(removeRecordBtn);
        enableButton(editRecordBtn);
    }

    /**
     * Метод переключает режим отображения меню
     *
     * @param editorMenu - меню редактирования
     * @param mainMenu - главное меню
     */
    private void switchMenus(Boolean editorMenu, Boolean mainMenu) {
        this.editorMenu.setVisible(editorMenu);
        this.mainMenu.setVisible(mainMenu);
    }

    /**
     * Метод включает кнопки
     *
     * @param button - кнопка
     */
    private void enableButton(HBox button) {
        button.setDisable(false);
    }


    /**
     * Метод отключает кнопки
     *
     * @param button - кнопка
     */
    private void disableBtn(HBox button) {
        button.setDisable(true);
    }

    /**
     * Метод возвращает отключенные кнопки в исходное состояние
     */
    private void resetDisableButtons() {
        enableButton(studentsBtn);
        enableButton(passportsDataBtn);
        enableButton(performancesBtn);
        enableButton(attendancesBtn);
        enableButton(birthCertificatesBtn);
        enableButton(parentsBtn);
        enableButton(editModeBtn);
        enableButton(disciplinesBtn);
    }

    /**
     * Метод возвращает текущую таблицу
     *
     * @return - таблица
     */
    private Tables getCurrentTable() {
        return tablesToDisplay.getCurrentTable();
    }

    /**
     * Метод устанаваливает обработчики событий на все кнопки
     */
    private void addHandlersAllButtons() {
        addHandlerForBtn(studentsBtn, studentsBtnHandler);
        addHandlerForBtn(passportsDataBtn, passportsDataBtnHandler);
        addHandlerForBtn(performancesBtn, performancesBtnHandler);
        addHandlerForBtn(attendancesBtn, attendancesBtnHandler);
        addHandlerForBtn(birthCertificatesBtn, birthCertificatesBtnHandler);
        addHandlerForBtn(parentsBtn, parentsBtnHandler);
        addHandlerForBtn(disciplinesBtn, disciplinesBtnHandler);
        addHandlerForBtn(editModeBtn, editorModeBtnHandler);
        addHandlerForBtn(exitBtn, exitBtnHandler);
        addHandlerForBtn(exitEditModeBtn, exitEditorModeBtnHandler);
        addHandlerForBtn(addRecordBtn, addRecordBtnHandler);
        addHandlerForBtn(removeRecordBtn, removeRecordBtnHandler);
        addHandlerForBtn(editRecordBtn, editRecordBtnHandler);
    }

    /**
     * Метод устанаваливает обработчик событий кнопку
     *
     * @param button - кнопка
     * @param eventHandler - обработчик события
     */
    private void addHandlerForBtn(HBox button, EventHandler<MouseEvent> eventHandler) {
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler);
    }

    /**
     * Метод переключает меню
     *
     * @throws NullPointerException - ошибка пустого объекта
     */
    private void switchMenu() throws NullPointerException {

        switch (getCurrentTable()) {

            case STUDENTS:

            case PARENTS:

            case ATTENDANCES:

            case DISCIPLINES:

            case PERFORMANCES:
                switchMenus(true, false);
                changeActivityEditButtons(false, false, false);
                break;

            case PASSPORTS_DATA:

            case BIRTH_CERTIFICATES:
                switchMenus(false, true);
                changeActivityEditButtons(true, true, false);
                break;
        }
    }

    /**
     * Метод открывает окно для редактирования данных
     *
     * @throws IOException - ошибка ввода вывода
     */
    private void openTableEditingWindow() throws IOException {

        switch (getCurrentTable()) {

            case STUDENTS:
                Windows.openStudentEditingWindow(tablesToDisplay);
                break;

            case PERFORMANCES:
                Windows.openPerformanceEditingWindow(tablesToDisplay);
                break;

            case ATTENDANCES:
                Windows.openAttendanceEditingWindow(tablesToDisplay);
                break;

            case DISCIPLINES:
                Windows.openDisciplineEditingWindow(tablesToDisplay);
                break;

            case PARENTS:
                Windows.openParentEditingWindow(tablesToDisplay);
                break;
        }
    }

    /**
     * Метод открывает окно для добавления данных
     *
     * @throws IOException - ошибка ввода вывода
     */
    private void openTableAddingWindow() throws IOException {

        switch (getCurrentTable()) {

            case STUDENTS:
                Windows.openStudentAddingWindow(tablesToDisplay);
                break;

            case PERFORMANCES:
                Windows.openPerformanceAddingWindow(tablesToDisplay);
                break;

            case ATTENDANCES:
                Windows.openAttendanceAddingWindow(tablesToDisplay);
                break;

            case DISCIPLINES:
                Windows.openDisciplineAddingWindow(tablesToDisplay);
                break;

            case PARENTS:
                Windows.openParentAddingWindow(tablesToDisplay);
                break;
        }
    }

    /**
     * Метод открывает окно для удаления данных
     *
     * @throws IOException - ошибка ввода вывода
     */
    private void openTableRemovalWindow() throws IOException {
        Windows.openRecordRemovalWindow(tablesToDisplay);
    }
}
