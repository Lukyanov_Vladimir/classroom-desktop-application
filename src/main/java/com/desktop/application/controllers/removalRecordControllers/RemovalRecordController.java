package com.desktop.application.controllers.removalRecordControllers;

import com.desktop.application.TablesToDisplay;
import com.desktop.application.services.*;
import com.desktop.application.utils.HibernateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;

/**
 * Класс контроллер для окна удаления записи
 */
public class RemovalRecordController {

    @FXML
    private TextField numberRecord;

    @FXML
    private Button removeBtn;

    private TablesToDisplay tablesToDisplay;

    /**
     * Метод устанавливает объект отображения таблиц
     *
     * @param tablesToDisplay - объект отображения таблиц
     */
    public void setTablesToDisplay(TablesToDisplay tablesToDisplay) {
        this.tablesToDisplay = tablesToDisplay;
    }

    @FXML
    public void initialize() {

        removeBtn.setOnAction(event -> {
            removeRecord();
            fieldClearText();
        });
    }

    /**
     * Метод удаляет запись
     */
    private void removeRecord() {
        Session session = HibernateUtil.openSession();

        switch (tablesToDisplay.getCurrentTable()) {

            case STUDENTS:
                StudentService studentService = new StudentService(session);
                studentService.delete(studentService.find(parseInt(numberRecord.getText())));
                tablesToDisplay.showTableStudents();
                break;

            case ATTENDANCES:
                AttendanceService attendanceService = new AttendanceService(session);
                attendanceService.delete(attendanceService.find(parseInt(numberRecord.getText())));
                tablesToDisplay.showTableAttendances();
                break;

            case PARENTS:
                ParentService parentService = new ParentService(session);
                parentService.delete(parentService.find(parseInt(numberRecord.getText())));
                tablesToDisplay.showTableParents();
                break;

            case DISCIPLINES:
                DisciplineService disciplineService = new DisciplineService(session);
                disciplineService.delete(disciplineService.find(parseInt(numberRecord.getText())));
                tablesToDisplay.showTableDisciplines();
                break;

            case BIRTH_CERTIFICATES:
                BirthCertificateService birthCertificateService = new BirthCertificateService(session);
                birthCertificateService.delete(birthCertificateService.find(parseInt(numberRecord.getText())));
                tablesToDisplay.showTableBirthCertificates();
                break;

            case PASSPORTS_DATA:
                PassportDataService passportDataService = new PassportDataService(session);
                passportDataService.delete(passportDataService.find(parseInt(numberRecord.getText())));
                tablesToDisplay.showTablePassportsData();
                break;

            case PERFORMANCES:
                PerformanceService performanceService = new PerformanceService(session);
                performanceService.delete(performanceService.find(parseInt(numberRecord.getText())));
                tablesToDisplay.showTablePerformances();
                break;
        }

        session.close();
    }

    /**
     * Метод преобразует строку в целое число
     *
     * @param str - строка
     * @return - целое число
     */
    private int parseInt(String str) {
        return Integer.parseInt(str);
    }

    /**
     * Метод очищает поле ввода данных
     */
    private void fieldClearText() {
        numberRecord.clear();
    }
}
