package com.desktop.application;

import com.desktop.application.controllers.addingRecordСontrollers.*;
import com.desktop.application.controllers.editingRecordControllers.*;
import com.desktop.application.controllers.removalRecordControllers.RemovalRecordController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Класс открытия окон
 */
public class Windows {

    /**
     * Метод открывает окно добаления данных студента
     *
     * @param tablesToDisplay - класс для отображения таблиц
     * @throws IOException - ошибка ввода и вывода
     */
    public static void openStudentAddingWindow(TablesToDisplay tablesToDisplay) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml/addingStudent.fxml"));

        AddingStudentController addingStudentController = new AddingStudentController();
        addingStudentController.setTablesToDisplay(tablesToDisplay);

        fxmlLoader.setController(addingStudentController);
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setTitle("Добавление студента");
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.getIcons().add(new Image("/img/icon.png"));
        stage.show();
    }

    /**
     * Метод открывает окно добаления данных родителя
     *
     * @param tablesToDisplay - класс для отображения таблиц
     * @throws IOException - ошибка ввода и вывода
     */
    public static void openParentAddingWindow(TablesToDisplay tablesToDisplay) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml/addingParent.fxml"));

        AddingParentController addingParentController = new AddingParentController();
        addingParentController.setTablesToDisplay(tablesToDisplay);

        fxmlLoader.setController(addingParentController);
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setTitle("Добавление родителя");
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.getIcons().add(new Image("/img/icon.png"));
        stage.show();
    }

    /**
     * Метод открывает окно добаления данных успеваемости
     *
     * @param tablesToDisplay - класс для отображения таблиц
     * @throws IOException - ошибка ввода и вывода
     */
    public static void openPerformanceAddingWindow(TablesToDisplay tablesToDisplay) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml/addingPerformance.fxml"));

        AddingPerformanceController addingPerformanceController = new AddingPerformanceController();
        addingPerformanceController.setTablesToDisplay(tablesToDisplay);

        fxmlLoader.setController(addingPerformanceController);
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setTitle("Добавление успеваемости");
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.getIcons().add(new Image("/img/icon.png"));
        stage.show();
    }

    /**
     * Метод открывает окно добаления данных дисцилины
     *
     * @param tablesToDisplay - класс для отображения таблиц
     * @throws IOException - ошибка ввода и вывода
     */
    public static void openDisciplineAddingWindow(TablesToDisplay tablesToDisplay) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml/addingDiscipline.fxml"));

        AddingDisciplineController addingDisciplineController = new AddingDisciplineController();
        addingDisciplineController.setTablesToDisplay(tablesToDisplay);
        ;

        fxmlLoader.setController(addingDisciplineController);
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setTitle("Добавление дисциплины");
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.getIcons().add(new Image("/img/icon.png"));
        stage.show();
    }

    /**
     * Метод открывает окно добаления данных посещаемости
     *
     * @param tablesToDisplay - класс для отображения таблиц
     * @throws IOException - ошибка ввода и вывода
     */
    public static void openAttendanceAddingWindow(TablesToDisplay tablesToDisplay) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml/addingAttendance.fxml"));

        AddingAttendanceController addingAttendanceController = new AddingAttendanceController();
        addingAttendanceController.setTablesToDisplay(tablesToDisplay);

        fxmlLoader.setController(addingAttendanceController);
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setTitle("Добавление посещаемости");
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.getIcons().add(new Image("/img/icon.png"));
        stage.show();
    }

    /**
     * Метод открывает окно редактирования данных студента
     *
     * @param tablesToDisplay - класс для отображения таблиц
     * @throws IOException - ошибка ввода и вывода
     */
    public static void openStudentEditingWindow(TablesToDisplay tablesToDisplay) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml/editingStudent.fxml"));

        EditingStudentController editingStudentController = new EditingStudentController();
        editingStudentController.setTablesToDisplay(tablesToDisplay);

        fxmlLoader.setController(editingStudentController);
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setTitle("Изменение данных студента");
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.getIcons().add(new Image("/img/icon.png"));
        stage.show();
    }

    /**
     * Метод открывает окно редактирования данных родителя
     *
     * @param tablesToDisplay - класс для отображения таблиц
     * @throws IOException - ошибка ввода и вывода
     */
    public static void openParentEditingWindow(TablesToDisplay tablesToDisplay) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml/editingParent.fxml"));

        EditingParentController editingParentController = new EditingParentController();
        editingParentController.setTablesToDisplay(tablesToDisplay);

        fxmlLoader.setController(editingParentController);
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setTitle("Изменение данных родителя");
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.getIcons().add(new Image("/img/icon.png"));
        stage.show();
    }

    /**
     * Метод открывает окно редактирования данных посещаемости
     *
     * @param tablesToDisplay - класс для отображения таблиц
     * @throws IOException - ошибка ввода и вывода
     */
    public static void openAttendanceEditingWindow(TablesToDisplay tablesToDisplay) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml/editingAttendance.fxml"));

        EditingAttendanceController editingAttendanceController = new EditingAttendanceController();
        editingAttendanceController.setTablesToDisplay(tablesToDisplay);

        fxmlLoader.setController(editingAttendanceController);
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setTitle("Изменение данных посещаемости");
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.getIcons().add(new Image("/img/icon.png"));
        stage.show();
    }

    /**
     * Метод открывает окно редактирования данных успеваемости
     *
     * @param tablesToDisplay - класс для отображения таблиц
     * @throws IOException - ошибка ввода и вывода
     */
    public static void openPerformanceEditingWindow(TablesToDisplay tablesToDisplay) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml/editingPerformance.fxml"));

        EditingPerformanceController editingPerformanceController = new EditingPerformanceController();
        editingPerformanceController.setTablesToDisplay(tablesToDisplay);

        fxmlLoader.setController(editingPerformanceController);
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setTitle("Изменение данных успеваемости");
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.getIcons().add(new Image("/img/icon.png"));
        stage.show();
    }

    /**
     * Метод открывает окно редактирования данных дисциплины
     *
     * @param tablesToDisplay - класс для отображения таблиц
     * @throws IOException - ошибка ввода и вывода
     */
    public static void openDisciplineEditingWindow(TablesToDisplay tablesToDisplay) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml/editingDiscipline.fxml"));

        EditingDisciplineController editingDisciplineController = new EditingDisciplineController();
        editingDisciplineController.setTablesToDisplay(tablesToDisplay);

        fxmlLoader.setController(editingDisciplineController);
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setTitle("Изменение данных успеваемости");
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.getIcons().add(new Image("/img/icon.png"));
        stage.show();
    }

    /**
     * Метод открывает окно удаления записи
     *
     * @param tablesToDisplay - класс для отображения таблиц
     * @throws IOException - ошибка ввода и вывода
     */
    public static void openRecordRemovalWindow(TablesToDisplay tablesToDisplay) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml/removalRecord.fxml"));

        RemovalRecordController removalRecordController = new RemovalRecordController();
        removalRecordController.setTablesToDisplay(tablesToDisplay);

        fxmlLoader.setController(removalRecordController);
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setTitle("Удаление записи");
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.getIcons().add(new Image("/img/icon.png"));
        stage.show();
    }

    /**
     * Метод открывает окно с текстом ошибки
     *
     * @param type - тип ошибки
     * @param title - заголовок окна
     * @param headerText - текст шапки
     * @param contentText - текст контентной части
     */
    public static void openInformationWindow(Alert.AlertType type, String title, String headerText, String contentText) {
        Alert alert = new Alert(type);

        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);

        alert.showAndWait();
    }
}
