package com.desktop.application;

import com.desktop.application.entities.*;
import com.desktop.application.services.*;
import com.desktop.application.utils.HibernateUtil;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.hibernate.Session;

import java.util.Date;
import java.util.List;

/**
 * Класс отображения таблиц
 */
public class TablesToDisplay {

    private final TableView<Student> mainTable;

    private final TableView<Performance> tablePerformances;

    private final TableView<Parent> tableParents;

    private final TableView<Attendance> tableAttendances;

    private final TableView<Discipline> tableDisciplines;

    private Tables currentTable;

    public TablesToDisplay(TableView<Student> mainTable, TableView<Performance> tablePerformances, TableView<Parent> tableParents, TableView<Attendance> tableAttendances, TableView<Discipline> tableDisciplines) {
        this.mainTable = mainTable;
        this.tablePerformances = tablePerformances;
        this.tableParents = tableParents;
        this.tableAttendances = tableAttendances;
        this.tableDisciplines = tableDisciplines;
    }

    /**
     * Метод возвращает текущую таблицу
     *
     * @return - текущая таблица
     */
    public Tables getCurrentTable() {
        return currentTable;
    }

    /**
     * Метод добавляет колонки в главную таблицу
     *
     * @param columns - колонки
     */
    @SafeVarargs
    private final void addColumnsInMainTable(TableColumn<Student, ?>... columns) {
        mainTable.getColumns().addAll(columns);
    }

    /**
     * Метод добавляет данные в главную таблицу
     *
     * @param observableList - список студентов
     */
    private void setItemsInMainTable(ObservableList<Student> observableList) {
        mainTable.setItems(observableList);
    }

    /**
     * Метод создаёт observableList из списка студентов
     *
     * @param students - список студентов
     * @return - observableList
     */
    private ObservableList<Student> getObservableListForMainTable(List<Student> students) {
        return FXCollections.observableList(students);
    }

    /**
     * Метод выводит данные в главную таблицу
     *
     * @param columns - колонки
     */
    @SafeVarargs
    private final void showData(TableColumn<Student, ?>... columns) {
        clearTable(mainTable);
        Session session = HibernateUtil.openSession();
        StudentService studentService = new StudentService(session);
        setItemsInMainTable(getObservableListForMainTable(studentService.findAll()));
        session.close();
        addColumnsInMainTable(columns);

        if (checkReturnTablesOriginalState(Tables.STUDENTS) ||
                checkReturnTablesOriginalState(Tables.BIRTH_CERTIFICATES) ||
                checkReturnTablesOriginalState(Tables.PASSPORTS_DATA))
            returnTablesInOriginalState();
    }

    /**
     * Метод устанваливает текущую таблицу
     *
     * @param table - таблица
     */
    private void setCurrentTable(Tables table) {
        currentTable = table;
    }

    /**
     * Метод отображает таблицу "Сутденты"
     */
    public void showTableStudents() {

        TableColumn<Student, Integer> numberStudent = new TableColumn<>("№");
        numberStudent.setCellValueFactory(new PropertyValueFactory<>("id"));
        numberStudent.setStyle("-fx-alignment: center");

        TableColumn<Student, String> fullName = new TableColumn<>("ФИО студента");
        fullName.setCellValueFactory(new PropertyValueFactory<>("fullName"));

        TableColumn<Student, Date> birthDate = new TableColumn<>("Дата рождения");
        birthDate.setCellValueFactory(new PropertyValueFactory<>("birthDate"));
        birthDate.setStyle("-fx-alignment: center");

        TableColumn<Student, Long> phoneNumber = new TableColumn<>("Номер телефона");
        phoneNumber.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        phoneNumber.setStyle("-fx-alignment: center");

        TableColumn<Student, String> address = new TableColumn<>("Адрес проживания");
        address.setCellValueFactory(param -> new SimpleObjectProperty<>(
                "г. " + param.getValue().getAddress().getCity() + ", ул. " +
                        param.getValue().getAddress().getStreet() + ", дом " +
                        param.getValue().getAddress().getHouseNumber() + ", кв. " +
                        param.getValue().getAddress().getFlatNumber()
        ));

        showData(numberStudent, fullName, birthDate, phoneNumber, address);

        setCurrentTable(Tables.STUDENTS);
    }

    /**
     * Метод отображает таблицу "Пасспротные данные"
     */
    public void showTablePassportsData() {

        TableColumn<Student, Integer> numberPassportData = new TableColumn<>("№");
        numberPassportData.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getPassportData().getId()
        ));
        numberPassportData.setStyle("-fx-alignment: center");

        TableColumn<Student, String> fullName = new TableColumn<>("ФИО студента");
        fullName.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getPassportData().getFullName()
        ));

        TableColumn<Student, Date> birthDate = new TableColumn<>("Дата рождения");
        birthDate.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getPassportData().getDateBirth()
        ));
        birthDate.setStyle("-fx-alignment: center");

        TableColumn<Student, String> address = new TableColumn<>("Место рождения");
        address.setCellValueFactory(param -> new SimpleObjectProperty<>(
                "г. " + param.getValue().getPassportData().getPlaceBirth().getCity() + ", ул. " +
                        param.getValue().getPassportData().getPlaceBirth().getStreet() + ", дом " +
                        param.getValue().getPassportData().getPlaceBirth().getHouseNumber() + ", кв. " +
                        param.getValue().getPassportData().getPlaceBirth().getFlatNumber()
        ));

        TableColumn<Student, Integer> series = new TableColumn<>("Серия");
        series.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getPassportData().getSeries()
        ));
        series.setStyle("-fx-alignment: center");

        TableColumn<Student, Integer> number = new TableColumn<>("Номер");
        number.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getPassportData().getNumber()
        ));
        number.setStyle("-fx-alignment: center");

        TableColumn<Student, String> issuedBy = new TableColumn<>("Кем выдан");
        issuedBy.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getPassportData().getIssuedBy()
        ));

        TableColumn<Student, Date> dateIssue = new TableColumn<>("Дата Выдачи");
        dateIssue.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getPassportData().getDateIssue()
        ));
        dateIssue.setStyle("-fx-alignment: center");

        TableColumn<Student, Long> departmentCode = new TableColumn<>("Код подразделения");
        departmentCode.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getPassportData().getDepartmentCode()
        ));
        departmentCode.setStyle("-fx-alignment: center");

        TableColumn<Student, Long> tin = new TableColumn<>("ИНН");
        tin.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getPassportData().getTin()
        ));
        tin.setStyle("-fx-alignment: center");

        TableColumn<Student, Long> snilsNumber = new TableColumn<>("СНИЛС");
        snilsNumber.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getPassportData().getSnilsNumber()
        ));
        snilsNumber.setStyle("-fx-alignment: center");

        showData(numberPassportData, fullName, birthDate, address, series, number, issuedBy,
                dateIssue, departmentCode, tin, snilsNumber);

        setCurrentTable(Tables.PASSPORTS_DATA);
    }

    /**
     * Метод отображает таблицу "Успеваемости"
     */
    public void showTablePerformances() {

        clearTable(tablePerformances);

        Session session = HibernateUtil.openSession();

        PerformanceService performanceService = new PerformanceService(session);
        tablePerformances.setItems(FXCollections.observableList(performanceService.findAll()));

        session.close();

        TableColumn<Performance, Integer> numberPerformance = new TableColumn<>("№");
        numberPerformance.setCellValueFactory(new PropertyValueFactory<>("id"));
        numberPerformance.setStyle("-fx-alignment: center");

        TableColumn<Performance, String> fullNameStudent = new TableColumn<>("ФИО студента");
        fullNameStudent.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getStudent().getFullName()
        ));

        TableColumn<Performance, Integer> mark = new TableColumn<>("Оценка");
        mark.setCellValueFactory(new PropertyValueFactory<>("mark"));
        mark.setStyle("-fx-alignment: center");

        TableColumn<Performance, String> discipline = new TableColumn<>("Дисциплина");
        discipline.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getDiscipline().getDisciplineName()
        ));

        TableColumn<Performance, String> teacher = new TableColumn<>("ФИО преподавателя");
        teacher.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getDiscipline().getFullNameTeacher()
        ));

        tablePerformances.getColumns().addAll(numberPerformance, fullNameStudent, mark, discipline, teacher);

        if (checkReturnTablesOriginalState(Tables.PERFORMANCES))
            returnTablesInOriginalState();

        if (!tablePerformances.isVisible())
            makeTableVisible(tablePerformances);

        setCurrentTable(Tables.PERFORMANCES);
    }

    /**
     * Метод отображает таблицу "Посещаемости"
     */
    public void showTableAttendances() {

        clearTable(tableAttendances);

        Session session = HibernateUtil.openSession();

        AttendanceService attendanceService = new AttendanceService(session);
        tableAttendances.setItems(FXCollections.observableList(attendanceService.findAll()));

        session.close();

        TableColumn<Attendance, Integer> numberAttendance = new TableColumn<>("№");
        numberAttendance.setCellValueFactory(new PropertyValueFactory<>("id"));
        numberAttendance.setStyle("-fx-alignment: center");

        TableColumn<Attendance, String> fullNameStudent = new TableColumn<>("ФИО студента");
        fullNameStudent.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getStudent().getFullName()
        ));

        TableColumn<Attendance, Integer> numberHours = new TableColumn<>("Кол-во часов");
        numberHours.setCellValueFactory(new PropertyValueFactory<>("numberHours"));
        numberHours.setStyle("-fx-alignment: center");

        TableColumn<Attendance, Date> date = new TableColumn<>("Дата");
        date.setCellValueFactory(new PropertyValueFactory<>("date"));

        tableAttendances.getColumns().addAll(numberAttendance, fullNameStudent, numberHours, date);


        if (checkReturnTablesOriginalState(Tables.ATTENDANCES))
            returnTablesInOriginalState();

        if (!tableAttendances.isVisible())
            makeTableVisible(tableAttendances);

        setCurrentTable(Tables.ATTENDANCES);
    }

    /**
     * Метод отображает таблицу "Свидительства о рождении"
     */
    public void showTableBirthCertificates() {

        TableColumn<Student, Integer> numberBirthCertificate = new TableColumn<>("№");
        numberBirthCertificate.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getBirthCertificate().getId()
        ));
        numberBirthCertificate.setStyle("-fx-alignment: center");

        TableColumn<Student, String> fullNameStudent = new TableColumn<>("ФИО студента");
        fullNameStudent.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getBirthCertificate().getStudent().getFullName()
        ));

        TableColumn<Student, Long> series = new TableColumn<>("Серия");
        series.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getBirthCertificate().getSeries()
        ));
        series.setStyle("-fx-alignment: center");

        TableColumn<Student, Long> number = new TableColumn<>("Номер");
        number.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getBirthCertificate().getNumber()
        ));
        number.setStyle("-fx-alignment: center");

        TableColumn<Student, String> issuedBy = new TableColumn<>("Кем выдан");
        issuedBy.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getBirthCertificate().getIssuedBy()
        ));

        TableColumn<Student, Date> dateIssue = new TableColumn<>("Дата выдачи");
        dateIssue.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getBirthCertificate().getDateIssue()
        ));
        dateIssue.setStyle("-fx-alignment: center");

        showData(numberBirthCertificate, fullNameStudent, series, number, issuedBy, dateIssue);

        setCurrentTable(Tables.BIRTH_CERTIFICATES);
    }

    /**
     * Метод отображает таблицу "Родители"
     */
    public void showTableParents() {

        clearTable(tableParents);

        Session session = HibernateUtil.openSession();

        ParentService parentService = new ParentService(session);
        tableParents.setItems(FXCollections.observableList(parentService.findAll()));

        session.close();

        TableColumn<Parent, Integer> numberParent = new TableColumn<>("№");
        numberParent.setCellValueFactory(new PropertyValueFactory<>("id"));
        numberParent.setStyle("-fx-alignment: center");

        TableColumn<Parent, String> fullNameStudent = new TableColumn<>("ФИО студента");
        fullNameStudent.setCellValueFactory(param -> new SimpleObjectProperty<>(
                param.getValue().getStudent().getFullName()
        ));

        TableColumn<Parent, String> fullName = new TableColumn<>("ФИО родителя");
        fullName.setCellValueFactory(new PropertyValueFactory<>("fullName"));

        TableColumn<Parent, String> familyStatus = new TableColumn<>("ФИО статус");
        familyStatus.setCellValueFactory(new PropertyValueFactory<>("familyStatus"));
        familyStatus.setStyle("-fx-alignment: center");

        TableColumn<Parent, Integer> numberChildren = new TableColumn<>("Кол-во детей");
        numberChildren.setCellValueFactory(new PropertyValueFactory<>("numberChildren"));
        numberChildren.setStyle("-fx-alignment: center");

        TableColumn<Parent, Long> phoneNumber = new TableColumn<>("Номер телефона");
        phoneNumber.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        phoneNumber.setStyle("-fx-alignment: center");

        TableColumn<Parent, String> placeWork = new TableColumn<>("Место работы");
        placeWork.setCellValueFactory(new PropertyValueFactory<>("placeWork"));

        TableColumn<Parent, String> address = new TableColumn<>("Адрес проживания");
        address.setCellValueFactory(param -> new SimpleObjectProperty<>(
                "г. " + param.getValue().getAddress().getCity() + ", ул. " +
                        param.getValue().getAddress().getStreet() + ", дом " +
                        param.getValue().getAddress().getHouseNumber() + ", кв. " +
                        param.getValue().getAddress().getFlatNumber()
        ));

        tableParents.getColumns().addAll(numberParent, fullNameStudent, fullName, familyStatus,
                numberChildren, phoneNumber, placeWork, address);

        if (checkReturnTablesOriginalState(Tables.PARENTS))
            returnTablesInOriginalState();

        if (!tableParents.isVisible())
            makeTableVisible(tableParents);

        setCurrentTable(Tables.PARENTS);
    }

    /**
     * Метод отображает таблицу "Дисциплины"
     */
    public void showTableDisciplines() {

        clearTable(tableDisciplines);

        Session session = HibernateUtil.openSession();

        DisciplineService disciplineService = new DisciplineService(session);
        tableDisciplines.setItems(FXCollections.observableList(disciplineService.findAll()));

        session.close();

        TableColumn<Discipline, Integer> numberDiscipline = new TableColumn<>("№");
        numberDiscipline.setCellValueFactory(new PropertyValueFactory<>("id"));
        numberDiscipline.setStyle("-fx-alignment: center");

        TableColumn<Discipline, String> nameDiscipline = new TableColumn<>("Название дисциплины");
        nameDiscipline.setCellValueFactory(new PropertyValueFactory<>("disciplineName"));

        TableColumn<Discipline, String> fullNameTeacher = new TableColumn<>("ФИО учителя");
        fullNameTeacher.setCellValueFactory(new PropertyValueFactory<>("fullNameTeacher"));

        tableDisciplines.getColumns().addAll(numberDiscipline, nameDiscipline, fullNameTeacher);

        if (checkReturnTablesOriginalState(Tables.DISCIPLINES))
            returnTablesInOriginalState();

        if (!tableDisciplines.isVisible())
            makeTableVisible(tableDisciplines);

        setCurrentTable(Tables.DISCIPLINES);
    }

    /**
     * Метод возвращает видимость таблиц в исходное состояние
     */
    private void returnTablesInOriginalState() {
        makeTableInvisible(tablePerformances);
        makeTableInvisible(tableAttendances);
        makeTableInvisible(tableParents);
        makeTableInvisible(tableDisciplines);
    }

    /**
     * Метод убирает видимость таблиц
     */
    private void makeTableInvisible(TableView<?> table) {
        table.setVisible(false);
    }

    /**
     * Метод восстанавливает видимость таблиц
     */
    private void makeTableVisible(TableView<?> table) {
        table.setVisible(true);
    }

    private boolean checkReturnTablesOriginalState(Tables table) {
        if (currentTable == null)
            return true;

        return !currentTable.equals(table);
    }

    private void clearTable(TableView<?> table) {
        table.getColumns().clear();
    }
}
