package com.desktop.application.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "disciplines")
public class Discipline implements Serializable {

    private static final long serialVersionUID = 7;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "discipline_name", nullable = false)
    private String disciplineName;

    @Column(name = "full_name_teacher", nullable = false)
    private String fullNameTeacher;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "discipline")
    private List<Performance> performances;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDisciplineName() {
        return disciplineName;
    }

    public void setDisciplineName(String disciplineName) {
        this.disciplineName = disciplineName;
    }

    public String getFullNameTeacher() {
        return fullNameTeacher;
    }

    public void setFullNameTeacher(String fullNameTeacher) {
        this.fullNameTeacher = fullNameTeacher;
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        this.performances = performances;
    }

    public void addPerformance(Performance performance) {
        performance.setDiscipline(this);
        this.performances.add(performance);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (obj == null || obj.getClass() != this.getClass())
            return false;

        Discipline discipline = (Discipline) obj;
        return id.equals(discipline.id) &&
                Objects.equals(disciplineName, discipline.disciplineName) &&
                Objects.equals(fullNameTeacher, discipline.fullNameTeacher);
    }
}
