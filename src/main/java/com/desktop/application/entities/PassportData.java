package com.desktop.application.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "passport_data")
public class PassportData implements Serializable {

    private static final long serialVersionUID = 4;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_birth", nullable = false)
    private Date dateBirth;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "place_birth_id", nullable = false)
    private Address placeBirth;

    @Column(name = "series", nullable = false)
    private Integer series;

    @Column(name = "number", nullable = false)
    private Integer number;

    @Column(name = "issued_by", nullable = false)
    private String issuedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_issue", nullable = false)
    private Date dateIssue;

    @Column(name = "department_code", nullable = false)
    private Long departmentCode;

    @Column(name = "tin", nullable = false)
    private Long tin;

    @Column(name = "snils_number", nullable = false)
    private Long snilsNumber;

    @OneToOne(mappedBy = "passportData")
    private Student student;

    public PassportData() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(Date dateBirth) {
        this.dateBirth = dateBirth;
    }

    public Address getPlaceBirth() {
        return placeBirth;
    }

    public void setPlaceBirth(Address placeBirth) {
        this.placeBirth = placeBirth;
    }

    public Integer getSeries() {
        return series;
    }

    public void setSeries(Integer series) {
        this.series = series;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    public Date getDateIssue() {
        return dateIssue;
    }

    public void setDateIssue(Date dateIssue) {
        this.dateIssue = dateIssue;
    }

    public Long getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(Long departmentCode) {
        this.departmentCode = departmentCode;
    }

    public Long getTin() {
        return tin;
    }

    public void setTin(Long tin) {
        this.tin = tin;
    }

    public Long getSnilsNumber() {
        return snilsNumber;
    }

    public void setSnilsNumber(Long snilsNumber) {
        this.snilsNumber = snilsNumber;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (obj == null || obj.getClass() != this.getClass())
            return false;

        PassportData passportData = (PassportData) obj;
        return id.equals(passportData.id) &&
                Objects.equals(fullName, passportData.fullName) &&
                Objects.equals(number, passportData.number) &&
                Objects.equals(dateBirth, passportData.dateBirth) &&
                Objects.equals(dateIssue, passportData.dateIssue) &&
                placeBirth.equals(passportData.placeBirth) &&
                Objects.equals(series, passportData.series) &&
                Objects.equals(number, passportData.number) &&
                Objects.equals(issuedBy, passportData.issuedBy) &&
                Objects.equals(dateIssue, passportData.dateIssue) &&
                Objects.equals(departmentCode, passportData.departmentCode) &&
                Objects.equals(dateIssue, passportData.dateIssue) &&
                Objects.equals(tin, passportData.tin) &&
                Objects.equals(snilsNumber, passportData.snilsNumber);
    }
}
