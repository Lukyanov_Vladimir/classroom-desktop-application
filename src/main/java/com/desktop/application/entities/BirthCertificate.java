package com.desktop.application.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "birth_certificates")
public class BirthCertificate implements Serializable {

    private static final long serialVersionUID = 5;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "series", nullable = false)
    private Long series;

    @Column(name = "number", nullable = false)
    private Long number;

    @Column(name = "issued_by", nullable = false)
    private String issuedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_issue", nullable = false)
    private Date dateIssue;

    @OneToOne(mappedBy = "birthCertificate")
    private Student student;

    public BirthCertificate() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getSeries() {
        return series;
    }

    public void setSeries(Long series) {
        this.series = series;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    public Date getDateIssue() {
        return dateIssue;
    }

    public void setDateIssue(Date dateIssue) {
        this.dateIssue = dateIssue;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (obj == null || obj.getClass() != this.getClass())
            return false;

        BirthCertificate birthCertificate = (BirthCertificate) obj;
        return id.equals(birthCertificate.id) &&
                Objects.equals(series, birthCertificate.series) &&
                Objects.equals(number, birthCertificate.number) &&
                Objects.equals(issuedBy, birthCertificate.issuedBy) &&
                dateIssue.equals(birthCertificate.dateIssue);
    }
}
