package com.desktop.application.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "students")
public class Student implements Serializable {

    private static final long serialVersionUID = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_birth", nullable = false)
    private Date birthDate;

    @Column(name = "phone_number", nullable = false)
    private Long phoneNumber;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", nullable = false)
    private Address address;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "passport_data_id", nullable = false)
    private PassportData passportData;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "birth_certificate_id", nullable = false)
    private BirthCertificate birthCertificate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private List<Parent> parents = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private List<Attendance> attendances = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private List<Performance> performances = new ArrayList<>();

    public Student() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public PassportData getPassportData() {
        return passportData;
    }

    public void setPassportData(PassportData passportData) {
        this.passportData = passportData;
    }

    public BirthCertificate getBirthCertificate() {
        return birthCertificate;
    }

    public void setBirthCertificate(BirthCertificate birthCertificate) {
        this.birthCertificate = birthCertificate;
    }

    public List<Parent> getParents() {
        return parents;
    }

    public void setParents(List<Parent> parents) {
        this.parents = parents;
    }

    public List<Attendance> getAttendances() {
        return attendances;
    }

    public void setAttendances(List<Attendance> attendances) {
        this.attendances = attendances;
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        this.performances = performances;
    }

    public void addParent(Parent parent) {
        parent.setStudent(this);
        parents.add(parent);
    }

    public void addAttendance(Attendance attendance) {
        attendance.setStudent(this);
        attendances.add(attendance);
    }

    public void addPerformance(Performance performance) {
        performance.setStudent(this);
        performances.add(performance);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (obj == null || obj.getClass() != this.getClass())
            return false;

        Student student = (Student) obj;
        return id.equals(student.id) &&
                Objects.equals(fullName, student.fullName) &&
                birthDate.equals(student.birthDate) &&
                Objects.equals(phoneNumber, student.phoneNumber) &&
                address.equals(student.address) &&
                passportData.equals(student.passportData) &&
                birthCertificate.equals(student.birthCertificate);
    }
}
