package com.desktop.application.services;

import com.desktop.application.dao.PerformanceDao;
import com.desktop.application.entities.Performance;
import org.hibernate.Session;

import java.util.List;

public class PerformanceService implements Service<Performance> {

    private final PerformanceDao performanceDao;

    public PerformanceService(Session session) {
        performanceDao = new PerformanceDao(session);
    }

    @Override
    public Performance find(int id) {
        return performanceDao.findById(id);
    }

    @Override
    public List<Performance> findAll() {
        return performanceDao.findAll();
    }

    @Override
    public void save(Performance performance) {
        performanceDao.save(performance);
    }

    @Override
    public void update(Performance performance) {
        performanceDao.update(performance);
    }

    @Override
    public void delete(Performance performance) {
        performanceDao.delete(performance);
    }
}
