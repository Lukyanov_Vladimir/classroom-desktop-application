package com.desktop.application.services;

import com.desktop.application.dao.StudentDao;
import com.desktop.application.entities.Student;
import org.hibernate.Session;

import java.util.List;

public class StudentService implements Service<Student> {

    private final StudentDao studentDao;

    public StudentService(Session session) {
        studentDao = new StudentDao(session);
    }

    @Override
    public Student find(int id) {
        return studentDao.findById(id);
    }

    @Override
    public List<Student> findAll() {
        return studentDao.findAll();
    }

    @Override
    public void save(Student student) {
        studentDao.save(student);
    }

    @Override
    public void update(Student student) {
        studentDao.update(student);
    }

    @Override
    public void delete(Student student) {
        studentDao.delete(student);
    }
}
