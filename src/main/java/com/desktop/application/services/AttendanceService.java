package com.desktop.application.services;

import com.desktop.application.dao.AttendanceDao;
import com.desktop.application.entities.Attendance;
import org.hibernate.Session;

import java.util.List;

public class AttendanceService implements Service<Attendance> {

    private final AttendanceDao attendanceDao;

    public AttendanceService(Session session) {
        attendanceDao = new AttendanceDao(session);
    }

    @Override
    public Attendance find(int id) {
        return attendanceDao.findById(id);
    }

    @Override
    public List<Attendance> findAll() {
        return attendanceDao.findAll();
    }

    @Override
    public void save(Attendance attendance) {
        attendanceDao.save(attendance);
    }

    @Override
    public void update(Attendance attendance) {
        attendanceDao.update(attendance);
    }

    @Override
    public void delete(Attendance attendance) {
        attendanceDao.delete(attendance);
    }
}
