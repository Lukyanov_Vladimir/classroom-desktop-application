package com.desktop.application.services;

import com.desktop.application.dao.DisciplineDao;
import com.desktop.application.entities.Discipline;
import org.hibernate.Session;

import java.util.List;

public class DisciplineService implements Service<Discipline> {

    private final DisciplineDao disciplineDao;

    public DisciplineService(Session session) {
        disciplineDao = new DisciplineDao(session);
    }

    @Override
    public Discipline find(int id) {
        return disciplineDao.findById(id);
    }

    @Override
    public List<Discipline> findAll() {
        return disciplineDao.findAll();
    }

    @Override
    public void save(Discipline discipline) {
        disciplineDao.save(discipline);
    }

    @Override
    public void update(Discipline discipline) {
        disciplineDao.update(discipline);
    }

    @Override
    public void delete(Discipline discipline) {
        disciplineDao.delete(discipline);
    }
}
