package com.desktop.application.services;

import com.desktop.application.dao.PassportDataDao;
import com.desktop.application.entities.PassportData;
import org.hibernate.Session;

import java.util.List;

public class PassportDataService implements Service<PassportData> {

    private final PassportDataDao passportDataDao;

    public PassportDataService(Session session) {
        passportDataDao = new PassportDataDao(session);
    }

    @Override
    public PassportData find(int id) {
        return passportDataDao.findById(id);
    }

    @Override
    public List<PassportData> findAll() {
        return passportDataDao.findAll();
    }

    @Override
    public void save(PassportData passportData) {
        passportDataDao.save(passportData);
    }

    @Override
    public void update(PassportData passportData) {
        passportDataDao.update(passportData);
    }

    @Override
    public void delete(PassportData passportData) {
        passportDataDao.delete(passportData);
    }
}
