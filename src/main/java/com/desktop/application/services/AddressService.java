package com.desktop.application.services;

import com.desktop.application.dao.AddressDao;
import com.desktop.application.entities.Address;
import org.hibernate.Session;

import java.util.List;

public class AddressService implements Service<Address> {

    private final AddressDao addressDao;

    public AddressService(Session session) {
        addressDao = new AddressDao(session);
    }

    @Override
    public Address find(int id) {
        return addressDao.findById(id);
    }

    @Override
    public List<Address> findAll() {
        return addressDao.findAll();
    }

    @Override
    public void save(Address address) {
        addressDao.save(address);
    }

    @Override
    public void update(Address address) {
        addressDao.update(address);
    }

    @Override
    public void delete(Address address) {
        addressDao.delete(address);
    }
}
