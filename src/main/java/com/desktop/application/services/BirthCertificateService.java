package com.desktop.application.services;

import com.desktop.application.dao.BirthCertificateDao;
import com.desktop.application.entities.BirthCertificate;
import org.hibernate.Session;

import java.util.List;

public class BirthCertificateService implements Service<BirthCertificate> {

    private final BirthCertificateDao birthCertificateDao;

    public BirthCertificateService(Session session) {
        birthCertificateDao = new BirthCertificateDao(session);
    }

    @Override
    public BirthCertificate find(int id) {
        return birthCertificateDao.findById(id);
    }

    @Override
    public List<BirthCertificate> findAll() {
        return birthCertificateDao.findAll();
    }

    @Override
    public void save(BirthCertificate birthCertificate) {
        birthCertificateDao.save(birthCertificate);
    }

    @Override
    public void update(BirthCertificate birthCertificate) {
        birthCertificateDao.update(birthCertificate);
    }

    @Override
    public void delete(BirthCertificate birthCertificate) {
        birthCertificateDao.delete(birthCertificate);
    }
}
