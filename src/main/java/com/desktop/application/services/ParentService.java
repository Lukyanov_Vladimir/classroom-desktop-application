package com.desktop.application.services;

import com.desktop.application.dao.ParentDao;
import com.desktop.application.entities.Parent;
import org.hibernate.Session;

import java.util.List;

public class ParentService implements Service<Parent> {

    private final ParentDao parentDao;

    public ParentService(Session session) {
        parentDao = new ParentDao(session);
    }

    @Override
    public Parent find(int id) {
        return parentDao.findById(id);
    }

    @Override
    public List<Parent> findAll() {
        return parentDao.findAll();
    }

    @Override
    public void save(Parent parent) {
        parentDao.save(parent);
    }

    @Override
    public void update(Parent parent) {
        parentDao.update(parent);
    }

    @Override
    public void delete(Parent parent) {
        parentDao.delete(parent);
    }
}
