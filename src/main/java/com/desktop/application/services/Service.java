package com.desktop.application.services;

import java.util.List;

public interface Service<T> {
    T find(int id);

    List<T> findAll();

    void save(T essence);

    void update(T essence);

    void delete(T essence);
}
