package com.desktop.application.services;

import com.desktop.application.CreatingDate;
import com.desktop.application.entities.*;
import com.desktop.application.utils.HibernateUtil;
import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;

public class ParentServiceTest {

    private Parent actualParent;

    @Test
    public void testCRUD() {
        HibernateUtil.buildSessionFactory();
        Session session = HibernateUtil.openSession();
        Student student = createStudent();
        StudentService studentService = new StudentService(session);
        studentService.save(student);
        actualParent = createParent(student);
        Assert.assertTrue(save(session));
        Assert.assertTrue(update(session));
        Assert.assertTrue(delete(session));
        studentService.delete(student);
        session.close();
        HibernateUtil.shutdown();
    }

    private boolean save(Session session) {

        ParentService parentService = new ParentService(session);

        parentService.save(actualParent);

        Parent expectedParent = parentService.find(actualParent.getId());

        return actualParent.equals(expectedParent);
    }


    private boolean update(Session session) {

        ParentService parentService = new ParentService(session);

        actualParent.setFullName("ФИО");
        actualParent.setFamilyStatus("Не полная");
        actualParent.setNumberChildren(2);
        actualParent.setPhoneNumber(22L);
        actualParent.setPlaceWork("Не работа");

        parentService.update(actualParent);

        Parent expectedParent = parentService.find(actualParent.getId());

        return actualParent.equals(expectedParent);
    }


    private boolean delete(Session session) {

        ParentService parentService = new ParentService(session);

        parentService.delete(actualParent);

        Parent parent = parentService.find(actualParent.getId());

        return parent == null;
    }

    private Parent createParent(Student student) {
        Parent parent = new Parent();
        parent.setFullName("Фио");
        parent.setFamilyStatus("Полная");
        parent.setNumberChildren(2);
        parent.setPhoneNumber(21412412L);
        parent.setPlaceWork("Работа");
        parent.setAddress(createAddress());
        parent.setStudent(student);

        return parent;
    }

    private Student createStudent() {
        Student student = new Student();
        student.setFullName("ФИО");
        student.setBirthDate(CreatingDate.createDate("2020-02-20"));
        student.setPhoneNumber(231233123L);
        student.setAddress(createAddress());
        student.setPassportData(createPassportData(student));
        student.setBirthCertificate(createBirthCertificate(student));

        return student;
    }

    private Address createAddress() {

        Address address = new Address();
        address.setCity("Город");
        address.setStreet("Улица");
        address.setHouseNumber(100);
        address.setFlatNumber(2);

        return address;
    }

    private BirthCertificate createBirthCertificate(Student student) {
        BirthCertificate birthCertificate = new BirthCertificate();
        birthCertificate.setSeries(124124L);
        birthCertificate.setNumber(214214L);
        birthCertificate.setIssuedBy("Выдано");
        birthCertificate.setDateIssue(CreatingDate.createDate("2020-02-20"));
        birthCertificate.setStudent(student);

        return birthCertificate;
    }

    private PassportData createPassportData(Student student) {
        PassportData passportData = new PassportData();
        passportData.setFullName("ФИО");
        passportData.setDateBirth(CreatingDate.createDate("2020-02-20"));
        passportData.setPlaceBirth(createAddress());
        passportData.setSeries(21421);
        passportData.setNumber(21341);
        passportData.setIssuedBy("Выдано");
        passportData.setDateIssue(CreatingDate.createDate("2020-02-20"));
        passportData.setDepartmentCode(2141242L);
        passportData.setTin(213L);
        passportData.setSnilsNumber(213L);
        passportData.setStudent(student);

        return passportData;
    }
}
