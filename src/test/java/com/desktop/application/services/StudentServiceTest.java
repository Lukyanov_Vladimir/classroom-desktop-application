package com.desktop.application.services;

import com.desktop.application.CreatingDate;
import com.desktop.application.entities.*;
import com.desktop.application.utils.HibernateUtil;
import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;

public class StudentServiceTest {

    private Student actualStudent;

    @Test
    public void testCRUD() {
        HibernateUtil.buildSessionFactory();
        Session session = HibernateUtil.openSession();
        actualStudent = createStudent();
        Assert.assertTrue(save(session));
        Assert.assertTrue(update(session));
        Assert.assertTrue(delete(session));
        session.close();
        HibernateUtil.shutdown();
    }

    private boolean save(Session session) {

        StudentService studentService = new StudentService(session);

        studentService.save(actualStudent);

        Student expectedStudent = studentService.find(actualStudent.getId());

        return actualStudent.equals(expectedStudent);
    }


    private boolean update(Session session) {

        StudentService studentService = new StudentService(session);

        actualStudent.setFullName("ФИО");
        actualStudent.setBirthDate(CreatingDate.createDate("2020-02-20"));
        actualStudent.setPhoneNumber(231233123L);

        studentService.update(actualStudent);

        Student expectedStudent = studentService.find(actualStudent.getId());

        return actualStudent.equals(expectedStudent);
    }


    private boolean delete(Session session) {

        StudentService studentService = new StudentService(session);

        studentService.delete(actualStudent);

        Student student = studentService.find(actualStudent.getId());

        return student == null;
    }

    private Student createStudent() {

        Student student = new Student();
        student.setFullName("ФИО");
        student.setBirthDate(CreatingDate.createDate("2020-02-20"));
        student.setPhoneNumber(231233123L);
        student.setAddress(createAddress());
        student.setPassportData(createPassportData(student));
        student.setBirthCertificate(createBirthCertificate(student));

        return student;
    }

    private Address createAddress() {

        Address address = new Address();
        address.setCity("Город");
        address.setStreet("Улица");
        address.setHouseNumber(100);
        address.setFlatNumber(2);

        return address;
    }

    private BirthCertificate createBirthCertificate(Student student) {
        BirthCertificate birthCertificate = new BirthCertificate();
        birthCertificate.setSeries(124124L);
        birthCertificate.setNumber(214214L);
        birthCertificate.setIssuedBy("Выдано");
        birthCertificate.setDateIssue(CreatingDate.createDate("2020-02-20"));
        birthCertificate.setStudent(student);

        return birthCertificate;
    }

    private PassportData createPassportData(Student student) {
        PassportData passportData = new PassportData();
        passportData.setFullName("ФИО");
        passportData.setDateBirth(CreatingDate.createDate("2020-02-20"));
        passportData.setPlaceBirth(createAddress());
        passportData.setSeries(21421);
        passportData.setNumber(21341);
        passportData.setIssuedBy("Выдано");
        passportData.setDateIssue(CreatingDate.createDate("2020-02-20"));
        passportData.setDepartmentCode(2141242L);
        passportData.setTin(213L);
        passportData.setSnilsNumber(213L);
        passportData.setStudent(student);

        return passportData;
    }
}