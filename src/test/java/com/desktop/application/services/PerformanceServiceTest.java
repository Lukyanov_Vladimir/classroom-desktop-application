package com.desktop.application.services;

import com.desktop.application.CreatingDate;
import com.desktop.application.entities.*;
import com.desktop.application.utils.HibernateUtil;
import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;

public class PerformanceServiceTest {

    private Performance actualPerformance;

    @Test
    public void testCRUD() {
        HibernateUtil.buildSessionFactory();
        Session session = HibernateUtil.openSession();

        Student student = createStudent();
        StudentService studentService = new StudentService(session);
        studentService.save(student);

        actualPerformance = createPerformance(student, session);
        Assert.assertTrue(save(session));
        Assert.assertTrue(update(session));
        Assert.assertTrue(delete(session));
        studentService.delete(student);
        session.close();
        HibernateUtil.shutdown();
    }

    private boolean save(Session session) {

        PerformanceService performanceService = new PerformanceService(session);

        performanceService.save(actualPerformance);

        Performance expectedPerformance = performanceService.find(actualPerformance.getId());

        return actualPerformance.equals(expectedPerformance);
    }


    private boolean update(Session session) {

        PerformanceService performanceService = new PerformanceService(session);

        actualPerformance.setMark(2);

        performanceService.update(actualPerformance);

        Performance expectedPerformance = performanceService.find(actualPerformance.getId());

        return actualPerformance.equals(expectedPerformance);
    }


    private boolean delete(Session session) {

        PerformanceService performanceService = new PerformanceService(session);

        performanceService.delete(actualPerformance);

        Performance performance = performanceService.find(actualPerformance.getId());

        return performance == null;
    }

    private Performance createPerformance(Student student, Session session) {

        Performance performance = new Performance();
        performance.setMark(5);
        performance.setStudent(student);
        DisciplineService disciplineService = new DisciplineService(session);

        Discipline discipline = disciplineService.find(1);
        discipline.addPerformance(performance);

        return performance;
    }

    private Student createStudent() {

        Student student = new Student();
        student.setFullName("ФИО");
        student.setBirthDate(CreatingDate.createDate("2020-02-20"));
        student.setPhoneNumber(231233123L);
        student.setAddress(createAddress());
        student.setPassportData(createPassportData(student));
        student.setBirthCertificate(createBirthCertificate(student));

        return student;
    }

    private Discipline createDiscipline() {
        Discipline discipline = new Discipline();
        discipline.setDisciplineName("Предмет");
        discipline.setFullNameTeacher("ФИО учителя");

        return discipline;
    }

    private Address createAddress() {

        Address address = new Address();
        address.setCity("Город");
        address.setStreet("Улица");
        address.setHouseNumber(100);
        address.setFlatNumber(2);

        return address;
    }

    private BirthCertificate createBirthCertificate(Student student) {
        BirthCertificate birthCertificate = new BirthCertificate();
        birthCertificate.setSeries(124124L);
        birthCertificate.setNumber(214214L);
        birthCertificate.setIssuedBy("Выдано");
        birthCertificate.setDateIssue(CreatingDate.createDate("2020-02-20"));
        birthCertificate.setStudent(student);

        return birthCertificate;
    }

    private PassportData createPassportData(Student student) {
        PassportData passportData = new PassportData();
        passportData.setFullName("ФИО");
        passportData.setDateBirth(CreatingDate.createDate("2020-02-20"));
        passportData.setPlaceBirth(createAddress());
        passportData.setSeries(21421);
        passportData.setNumber(21341);
        passportData.setIssuedBy("Выдано");
        passportData.setDateIssue(CreatingDate.createDate("2020-02-20"));
        passportData.setDepartmentCode(2141242L);
        passportData.setTin(213L);
        passportData.setSnilsNumber(213L);
        passportData.setStudent(student);

        return passportData;
    }
}