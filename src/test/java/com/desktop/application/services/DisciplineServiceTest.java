package com.desktop.application.services;

import com.desktop.application.entities.Discipline;
import com.desktop.application.utils.HibernateUtil;
import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;

public class DisciplineServiceTest {
    private Discipline actualDiscipline;

    @Test
    public void testCRUD() {
        HibernateUtil.buildSessionFactory();
        Session session = HibernateUtil.openSession();
        actualDiscipline = createDiscipline();
        Assert.assertTrue(save(session));
        Assert.assertTrue(update(session));
        Assert.assertTrue(delete(session));
        session.close();
        HibernateUtil.shutdown();
    }

    private boolean save(Session session) {

        DisciplineService disciplineService = new DisciplineService(session);

        disciplineService.save(actualDiscipline);

        Discipline expectedDiscipline = disciplineService.find(actualDiscipline.getId());

        return actualDiscipline.equals(expectedDiscipline);
    }

    private boolean update(Session session) {

        DisciplineService disciplineService = new DisciplineService(session);

        actualDiscipline.setDisciplineName("Дисциплина");
        actualDiscipline.setFullNameTeacher("Учитель");

        disciplineService.update(actualDiscipline);

        Discipline expectedDiscipline = disciplineService.find(actualDiscipline.getId());

        return actualDiscipline.equals(expectedDiscipline);
    }

    private boolean delete(Session session) {

        DisciplineService disciplineService = new DisciplineService(session);

        disciplineService.delete(actualDiscipline);

        Discipline discipline = disciplineService.find(actualDiscipline.getId());

        return discipline == null;
    }

    private Discipline createDiscipline() {
        Discipline discipline = new Discipline();
        discipline.setDisciplineName("Предмет");
        discipline.setFullNameTeacher("ФИО учителя");

        return discipline;
    }
}