package com.desktop.application.services;

import com.desktop.application.entities.Address;
import com.desktop.application.utils.HibernateUtil;
import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;

public class AddressServiceTest {

    private Address actualAddress;

    @Test
    public void testCRUD() {
        HibernateUtil.buildSessionFactory();
        Session session = HibernateUtil.openSession();
        actualAddress = createAddress();
        Assert.assertTrue(save(session));
        Assert.assertTrue(update(session));
        Assert.assertTrue(delete(session));
        session.close();
        HibernateUtil.shutdown();
    }

    private boolean save(Session session) {

        AddressService addressService = new AddressService(session);

        addressService.save(actualAddress);

        Address expectedAddress = addressService.find(actualAddress.getId());

        return actualAddress.equals(expectedAddress);
    }


    private boolean update(Session session) {

        AddressService addressService = new AddressService(session);

        actualAddress.setCity("Не город");
        actualAddress.setStreet("Не улица");
        actualAddress.setHouseNumber(50);
        actualAddress.setFlatNumber(1);

        addressService.update(actualAddress);

        Address expectedAddress = addressService.find(actualAddress.getId());

        return actualAddress.equals(expectedAddress);
    }


    private boolean delete(Session session) {

        AddressService addressService = new AddressService(session);

        addressService.delete(actualAddress);

        Address address = addressService.find(actualAddress.getId());

        return address == null;
    }

    private Address createAddress() {

        Address address = new Address();
        address.setCity("Город");
        address.setStreet("Улица");
        address.setHouseNumber(100);
        address.setFlatNumber(2);

        return address;
    }
}